import 'package:flightbooking/src/utils/constants.dart';
import 'package:flutter/material.dart';

class AppTheme {
  get lightTheme => ThemeData(
        brightness: Brightness.light,
        visualDensity: VisualDensity(vertical: 0.5, horizontal: 0.5),
        primarySwatch: MaterialColor(
          0xFFF5E0C3,
          <int, Color>{
            50: Color(0xFF0085FF),
            100: Color(0xFF0028F8),
            200: Color(0xaaF5E0C3),
            300: Color(0xafF5E0C3),
            400: Color(0xffF5E0C3),
            500: Color(0xffEDD5B3),
            600: Color(0xffDEC29B),
            700: Color(0xffC9A87C),
            800: Color(0xffB28E5E),
            900: Color(0xff936F3E)
          },
        ),
        scaffoldBackgroundColor: Constants.kitGradients[11],
        canvasColor: Constants.kitGradients[11],
        primaryColor: Constants.kitGradients[25],
        accentColor: Constants.kitGradients[13],
        cardColor: Constants.kitGradients[11],
        primaryColorLight: Constants.kitGradients[1],
        primaryColorDark: Constants.kitGradients[25],
        bottomAppBarColor: Constants.kitGradients[11],
        primaryColorBrightness: Brightness.light,
        accentColorBrightness: Brightness.light,
        dividerColor: Color(0xEDEDED),
        focusColor: Color(0x1aF5E0C3),
        buttonColor: Colors.black,
        textSelectionColor: Color(0xFF000837),
        hoverColor: Constants.kitGradients[4],
        inputDecorationTheme: InputDecorationTheme(
          fillColor: Colors.white,
        ),
      );

  get darkTheme => ThemeData(
      brightness: Brightness.dark,
      visualDensity: VisualDensity(vertical: 0.5, horizontal: 0.5),
      primarySwatch: MaterialColor(
        0xFFF5E0C3,
        <int, Color>{
          50: Color(0x1a5D4524),
          100: Color(0xa15D4524),
          200: Color(0xaa5D4524),
          300: Color(0xaf5D4524),
          400: Color(0x1a483112),
          500: Color(0xa1483112),
          600: Color(0xaa483112),
          700: Color(0xff483112),
          800: Color(0xaf2F1E06),
          900: Color(0xff2F1E06)
        },
      ),
      scaffoldBackgroundColor: Constants.kitGradients[13],
      canvasColor: Constants.kitGradients[13],
      primaryColor: Constants.kitGradients[13],
      accentColor: Constants.kitGradients[11],
      cardColor: Constants.kitGradients[16],
      primaryColorLight: Constants.kitGradients[11],
      primaryColorDark: Constants.kitGradients[16],
      bottomAppBarColor: Constants.kitGradients[16],
      primaryColorBrightness: Brightness.dark,
      accentColorBrightness: Brightness.dark,
      dividerColor: Colors.white10,
      focusColor: Color(0x1a311F06),
      buttonColor: Color(0xFF1C1F21),
      textSelectionColor: Colors.white,
      hoverColor: Colors.white,
      inputDecorationTheme: InputDecorationTheme(fillColor: Color(0xFF232323)));
}
