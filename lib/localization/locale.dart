import 'package:flightbooking/localization/demo_local.dart';
import 'package:flutter/material.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
