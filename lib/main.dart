import 'package:firebase_core/firebase_core.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';
import 'app/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Firebase.initializeApp();
  await Hive.openBox('box');
   await Hive.openBox('date');
   await Hive.openBox('calender');
   await Hive.openBox('code');
   await Hive.openBox('adult');
  runApp(ChangeNotifierProvider<ThemeNotifier>(
    create: (BuildContext context) {
      String theme = ObjectFactory().hiveBox.getCurrentTheme();
      print( "CurrentTheme" + theme.toString());
      if (theme == null ||
          theme == "" ||
          theme == "system default") {
        ObjectFactory().hiveBox.putCurrentTheme(theme: "system default");
        return ThemeNotifier(ThemeMode.system);
      }
      return ThemeNotifier(
          theme == "Dark" ? ThemeMode.dark : ThemeMode.light);
    },
    child: MyApp(),
  ),);
}
