import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[0],
      ),
      child: Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 2.1),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    Constants.kitGradients[0].withOpacity(0.85),
                    Constants.kitGradients[0].withOpacity(0.9),
                    Constants.kitGradients[0],
                    Constants.kitGradients[0],
                  ],
                  tileMode: TileMode.repeated,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Spacer(
                    flex: 1,
                  ),
                  Container(
                    height: screenWidth(context, dividedBy: 5),
                    width: screenWidth(context, dividedBy: 5),
                    child: SvgPicture.asset(
                      "assets/icons/splash_icon.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                  // Container(
                  //   height: screenWidth(context, dividedBy: 15),
                  //   width: screenWidth(context, dividedBy: 2),
                  //   child: SvgPicture.asset(
                  //     "assets/icons/all_flight_booking.svg",
                  //     fit: BoxFit.fill,
                  //   ),
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Container(
                    height: screenWidth(context, dividedBy: 20),
                    width: screenWidth(context, dividedBy: 2),
                    child: SvgPicture.asset(
                      "assets/icons/all_flight_booking.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
            ),
            Spacer(
              flex: 1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: screenWidth(context, dividedBy: 10),
                  width: screenWidth(context, dividedBy: 3),
                  color: Constants.kitGradients[26],
                  child: Center(
                    child: Text(
                      "About Us",
                      style: TextStyle(
                          color: Constants.kitGradients[27],
                          fontFamily: "AirbnbCerealAppRegular",
                          fontWeight: FontWeight.w700,
                          fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
            Spacer(
              flex: 1,
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                  width: screenHeight(context, dividedBy: 1.3),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting",
                    style: TextStyle(height: 2),
                  )),
            ),
            Spacer(
              flex: 10,
            ),
            // Stack(
            //   children: [
            //     Positioned(
            //       top: 0.0,
            //       left: 0.0,
            //       right: 0.0,
            //       child: Column(
            //         mainAxisAlignment: MainAxisAlignment.start,
            //         crossAxisAlignment: CrossAxisAlignment.start,
            //         children: [
            //           SizedBox(
            //             height: screenHeight(context, dividedBy: 47),
            //           ),
            //           IconButton(
            //               color: Constants.kitGradients[3],
            //               icon: Icon(
            //                 Icons.clear,
            //                 color: Theme.of(context).accentColor,
            //               ),
            //               onPressed: () {
            //                 Navigator.pushAndRemoveUntil(
            //                     context,
            //                     MaterialPageRoute(builder: (context) => HomePage()),
            //                         (route) => false);
            //               }),
            //           SizedBox(
            //             height: screenHeight(context, dividedBy: 45),
            //           ),
            //           Row(
            //             mainAxisAlignment: MainAxisAlignment.center,
            //             crossAxisAlignment: CrossAxisAlignment.start,
            //             children: [
            //               Column(
            //                 mainAxisAlignment: MainAxisAlignment.start,
            //                 crossAxisAlignment: CrossAxisAlignment.start,
            //                 children: [
            //                   SvgPicture.asset(
            //                     'assets/images/about_us_icon1.svg',
            //                     color: Constants.kitGradients[0],
            //                     width: screenWidth(context, dividedBy: 1.5),
            //                     height: screenHeight(context, dividedBy: 11),
            //                   ),
            //                   SizedBox(
            //                     height: screenHeight(context, dividedBy: 67),
            //                   ),
            //                   Text(
            //                     'cheap flights',
            //                     style: TextStyle(
            //                         fontSize: 16.0,
            //                         fontWeight: FontWeight.w400,
            //                         color: Constants.kitGradients[0],
            //                         fontFamily: 'ModernaFamily'),
            //                   ),
            //                 ],
            //               )
            //             ],
            //           ),
            //           SizedBox(
            //             height: screenHeight(context, dividedBy: 17),
            //           ),
            //           Padding(
            //             padding: EdgeInsets.only(
            //                 left: screenWidth(context, dividedBy: 20)),
            //             child: Text(
            //               'About us',
            //               style: TextStyle(
            //                   fontSize: 24.0,
            //                   fontWeight: FontWeight.w700,
            //                   color: Constants.kitGradients[0],
            //                   fontFamily: 'AirbnbCerealAppRegular'),
            //             ),
            //           ),
            //           Padding(
            //             padding: EdgeInsets.only(
            //                 left: screenWidth(context, dividedBy: 20)),
            //             child: Container(
            //               width: screenWidth(context, dividedBy: 15),
            //               height: 5.0,
            //               color: Constants.kitGradients[16],
            //             ),
            //           ),
            //           SizedBox(
            //             height: screenHeight(context, dividedBy: 35),
            //           ),
            //           Padding(
            //             padding: EdgeInsets.only(
            //                 left: screenWidth(context, dividedBy: 20),
            //                 right: screenWidth(context, dividedBy: 35)),
            //             child: Container(
            //               width: screenWidth(context, dividedBy: 1),
            //               child: Text(
            //                 Constants.ABOUT_US_NOTE,
            //                 overflow: TextOverflow.visible,
            //                 style: TextStyle(
            //                     fontSize: 12.0,
            //                     fontWeight: FontWeight.w700,
            //                     color: Theme.of(context).accentColor,
            //                     fontFamily: 'AirbnbCerealAppRegular'),
            //               ),
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //     Positioned(
            //       bottom: 10.0,
            //       left: 0.0,
            //       right: 0.0,
            //       child: Container(
            //         width: screenWidth(context, dividedBy: 1),
            //         height: screenHeight(context, dividedBy: 3),
            //         child: SvgPicture.asset(
            //           'assets/images/about_us_icon2.svg',
            //           color: Constants.kitGradients[0],
            //           fit: BoxFit.fill,
            //         ),
            //       ),
            //     )
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
