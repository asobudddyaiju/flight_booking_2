import 'package:address_picker/address_picker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/models/destination_from_response.dart';
import 'package:flightbooking/src/models/nearby_airport_request.dart';
import 'package:flightbooking/src/models/nearby_airport_response.dart';
import 'package:flightbooking/src/screens/home_page.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../bloc/app_bloc.dart';
import '../utils/object_factory.dart';

class DestinationFrom extends StatefulWidget {
  bool destinationTo;

  DestinationFrom({this.destinationTo});

  @override
  _DestinationFromState createState() => _DestinationFromState();
}

class _DestinationFromState extends State<DestinationFrom> {
  TextEditingController _controller = TextEditingController();
  AppBloc appBloc = AppBloc();
  String imagePathString;
  int chooseStream;
  List<Color> colorArray = [
    Constants.kitGradients[0],
    Constants.kitGradients[15]
  ];
  Color codeColor;

  bool showLoadingForCurrentLocationClick;

@override
  void initState() {
  appBloc.nearbySearchResponse.listen((event) {
    setState(() {
      showLoadingForCurrentLocationClick = false;
    });
  });
   setState(() {
     showLoadingForCurrentLocationClick = false;
   });
    super.initState();
  }

  @override
  void dispose() {
    appBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 5),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  Constants.kitGradients[0].withOpacity(0.85),
                  Constants.kitGradients[0].withOpacity(0.9),
                  Constants.kitGradients[0],
                  Constants.kitGradients[0],
                ],
                tileMode: TileMode.repeated,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 80),
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomePage()),
                              (route) => false);
                        },
                        child: Container(
                            width: screenWidth(context, dividedBy: 10),
                            height: screenHeight(context, dividedBy: 26),
                            child: Icon(
                              Icons.clear,
                              color: Constants.kitGradients[11],
                            ))),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    Text(
                      widget.destinationTo == false
                          ? getTranslated(context, "From")
                          : getTranslated(context, "To"),
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          color: Constants.kitGradients[11],
                          fontSize: 20,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1.1),
                      height: screenHeight(context, dividedBy: 15),
                      decoration: BoxDecoration(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          borderRadius: BorderRadius.circular(5)),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Row(
                          children: [
                            Container(
                              width: screenWidth(context, dividedBy: 1.3),
                              child: TextField(
                                maxLines: 1,
                                decoration: InputDecoration(
                                  hintText: widget.destinationTo == false
                                      ? getTranslated(
                                          context, "Search_destination_From")
                                      : getTranslated(
                                          context, "Search_destination_To"),
                                  hintStyle: TextStyle(
                                      color: Constants.kitGradients[1],
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.0,
                                      fontFamily: 'Roboto'),
                                  border: InputBorder.none,
                                ),
                                controller: _controller,
                                onChanged: (_) {
                                  setState(() {
                                    chooseStream = 0;
                                  });
                                  appBloc.destinationFromCall(
                                      searchKey: _controller.text.toString());
                                },
                              ),
                            ),
                            GestureDetector(
                              child: Icon(Icons.clear,
                                  color: Constants.kitGradients[0]),
                              onTap: () {
                                _controller.clear();
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          widget.destinationTo == false
              ? GestureDetector(
                  child: Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      SvgPicture.asset(
                          'assets/icons/destination_page_arrow.svg',
                      color: Constants.kitGradients[0],),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Text(
                        getTranslated(context, "Current_location"),
                        style: TextStyle(
                            fontFamily: 'AirbnbCerealAppRegular',
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w700,
                            fontSize: 14,
                            color: Theme.of(context).accentColor),
                      ),
                    ],
                  ),
                  onTap: () {
                    FirebaseAnalytics().logEvent(
                        name: "clicked_current_location", parameters: null);
                    appBloc.searchNearbyAirports(
                        nearByAirportsRequest: NearByAirportsRequest(
                            lat: ObjectFactory().hiveBox.hiveGet(key: "lat"),
                            lon: ObjectFactory().hiveBox.hiveGet(key: "lon"),
                            locationTypes: "airport",
                            limit: 5,
                            locale: "en",
                            radius: "250",
                            sort: "name"));
                    setState(() {
                      chooseStream = 1;
                      showLoadingForCurrentLocationClick = true;
                    });
                  },
                )
              : Container(),
          widget.destinationTo == false
              ? SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                )
              : Container(),
          widget.destinationTo == false
              ? Container(
                  height: 0.5,
                  width: screenWidth(context, dividedBy: 1),
                  color: Constants.kitGradients[17],
                )
              : Container(),
          chooseStream == 0
              ? StreamBuilder<DestinationFromResponse>(
                  stream: appBloc.destinationResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: snapshot.data.resultsRetrieved,
                                itemBuilder: (BuildContext ctxt, int Index) {
                                  if (snapshot.data.locations[Index].type ==
                                      "airport") {
                                    codeColor = colorArray[0];
                                    imagePathString =
                                        'assets/icons/destination_page_icon.svg';
                                  } else {
                                    codeColor = colorArray[1];
                                    imagePathString =
                                        'assets/icons/destination_page_city.svg';
                                  }
                                  return snapshot.data.locations[Index].code !=
                                          null
                                      ? GestureDetector(
                                          onTap: () async {
                                            FirebaseAnalytics().logEvent(
                                                name: "destination_selected",
                                                parameters: {
                                                  "destination": snapshot.data
                                                      .locations[Index].name
                                                      .toString()
                                                });
                                            widget.destinationTo == false
                                                ? ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key: 'destinationFrom',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code)
                                                : ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key: 'destinationTo',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code);

                                            widget.destinationTo == false
                                                ? ObjectFactory().hiveBox.hivePut(
                                                    key: 'destinationFromName',
                                                    value: snapshot.data
                                                        .locations[Index].name)
                                                : ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key:
                                                            'destinationToName',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .name);

                                            Navigator.pushAndRemoveUntil(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()),
                                                (route) => false);
                                          },
                                          child: Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 12),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        SvgPicture.asset(
                                                            imagePathString,),
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          1.4),
                                                              child: RichText(
                                                                text: TextSpan(
                                                                    text: snapshot
                                                                                .data
                                                                                .locations[
                                                                                    Index]
                                                                                .name !=
                                                                            null
                                                                        ? snapshot
                                                                            .data
                                                                            .locations[
                                                                                Index]
                                                                            .name
                                                                            .substring(
                                                                                0,
                                                                                _controller
                                                                                    .text.length)
                                                                        : " ",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'AirbnbCerealAppRegular',
                                                                        color: Constants.kitGradients[
                                                                            0],
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w700,
                                                                        fontSize:
                                                                            16),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                        text: snapshot.data.locations[Index].name !=
                                                                                null
                                                                            ? snapshot.data.locations[Index].name.replaceRange(
                                                                                0,
                                                                                _controller.text.length,
                                                                                "")
                                                                            : " ",
                                                                        style: TextStyle(
                                                                            fontFamily:
                                                                                'AirbnbCerealAppRegular',
                                                                            fontWeight:
                                                                                FontWeight.w700,
                                                                            color: Theme.of(context).accentColor,
                                                                            fontSize: 16),
                                                                      )
                                                                    ]),
                                                              ),
                                                            ),
                                                            snapshot
                                                                        .data
                                                                        .locations[
                                                                            Index]
                                                                        .country !=
                                                                    null
                                                                ? Text(
                                                                    snapshot
                                                                            .data
                                                                            .locations[
                                                                                Index]
                                                                            .country
                                                                            .name +
                                                                        ", " +
                                                                        snapshot
                                                                            .data
                                                                            .locations[Index]
                                                                            .continent
                                                                            .name,
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'AirbnbCerealAppRegular',
                                                                        fontStyle:
                                                                            FontStyle
                                                                                .normal,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            8,
                                                                        color: Constants
                                                                            .kitGradients[17]),
                                                                  )
                                                                : Text(
                                                                    snapshot
                                                                            .data
                                                                            .locations[
                                                                                Index]
                                                                            .city
                                                                            .name +
                                                                        ", " +
                                                                        snapshot
                                                                            .data
                                                                            .locations[
                                                                                Index]
                                                                            .city
                                                                            .country
                                                                            .name +
                                                                        ", " +
                                                                        snapshot
                                                                            .data
                                                                            .locations[Index]
                                                                            .city
                                                                            .continent
                                                                            .name,
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'AirbnbCerealAppRegular',
                                                                        fontStyle:
                                                                            FontStyle
                                                                                .normal,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            8,
                                                                        color: Constants
                                                                            .kitGradients[17]),
                                                                  ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 10.0),
                                                      child: Text(
                                                        snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'AirbnbCerealAppRegular',
                                                            fontStyle: FontStyle
                                                                .normal,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 12,
                                                            color: codeColor),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  height: 0.5,
                                                  width: screenWidth(context,
                                                      dividedBy: 1),
                                                  color: Constants
                                                      .kitGradients[17],
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      : Container();
                                }))
                        : _controller.text.length > 0
                            ? Center(
                                child: Container(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(Constants
                                    .kitGradients[0],),
                                ),
                              ))
                            : Container();
                  })
              : StreamBuilder<NearByAirportsResponse>(
                  stream: appBloc.nearbySearchResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: snapshot.data.resultsRetrieved,
                                itemBuilder: (BuildContext ctxt, int Index) {
                                  if (snapshot.data.locations[Index].type ==
                                      "airport") {
                                    codeColor = colorArray[0];
                                    imagePathString =
                                        'assets/icons/destination_page_icon.svg';
                                  } else {
                                    codeColor = colorArray[1];
                                    imagePathString =
                                        'assets/icons/destination_page_city.svg';
                                  }
                                  return snapshot.data.locations[Index].code !=
                                          null
                                      ? GestureDetector(
                                          onTap: () async {
                                            widget.destinationTo == false
                                                ? ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key: 'destinationFrom',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code)
                                                : ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key: 'destinationTo',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code);

                                            widget.destinationTo == false
                                                ? ObjectFactory().hiveBox.hivePut(
                                                    key: 'destinationFromSwap',
                                                    value: snapshot.data
                                                        .locations[Index].code)
                                                : ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key:
                                                            'destinationToSwap',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code);

                                            widget.destinationTo == false
                                                ? ObjectFactory().hiveBox.hivePut(
                                                    key: 'destinationFromName',
                                                    value: snapshot.data
                                                        .locations[Index].name)
                                                : ObjectFactory()
                                                    .hiveBox
                                                    .hivePut(
                                                        key:
                                                            'destinationToName',
                                                        value: snapshot
                                                            .data
                                                            .locations[Index]
                                                            .name);

                                            widget.destinationTo == false
                                                ? ObjectFactory().hiveBox.hivePut(
                                                    key:
                                                        'destinationFromNameSwap',
                                                    value: snapshot.data
                                                        .locations[Index].name)
                                                : ObjectFactory().hiveBox.hivePut(
                                                    key:
                                                        'destinationToNameSwap',
                                                    value: snapshot.data
                                                        .locations[Index].name);

                                            Navigator.pushAndRemoveUntil(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        HomePage()),
                                                (route) => false);
                                          },
                                          child: Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 12),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        SvgPicture.asset(
                                                            imagePathString),
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          1.4),
                                                              child: Text(
                                                                snapshot
                                                                            .data
                                                                            .locations[
                                                                                Index]
                                                                            .name !=
                                                                        null
                                                                    ? snapshot
                                                                        .data
                                                                        .locations[
                                                                            Index]
                                                                        .name
                                                                    : " ",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'AirbnbCerealAppRegular',
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                    fontSize:
                                                                        14,
                                                                    color: Constants
                                                                            .kitGradients[
                                                                        13]),
                                                              ),
                                                            ),
                                                            Text(
                                                              snapshot
                                                                      .data
                                                                      .locations[
                                                                          Index]
                                                                      .city
                                                                      .name +
                                                                  ", " +
                                                                  snapshot
                                                                      .data
                                                                      .locations[
                                                                          Index]
                                                                      .city
                                                                      .country
                                                                      .name +
                                                                  ", " +
                                                                  snapshot
                                                                      .data
                                                                      .locations[
                                                                          Index]
                                                                      .city
                                                                      .continent
                                                                      .name,
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'AirbnbCerealAppRegular',
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 8,
                                                                  color: Constants
                                                                          .kitGradients[
                                                                      17]),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 10.0),
                                                      child: Text(
                                                        snapshot
                                                            .data
                                                            .locations[Index]
                                                            .code,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'AirbnbCerealAppRegular',
                                                            fontStyle: FontStyle
                                                                .normal,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 12,
                                                            color: codeColor),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  height: 0.5,
                                                  width: screenWidth(context,
                                                      dividedBy: 1),
                                                  color: Constants
                                                      .kitGradients[17],
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      : Container();
                                }))
                        : (showLoadingForCurrentLocationClick == true?(Center(
                        child: Container(
                          child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(Constants
                                .kitGradients[0],),
                          ),
                        ))):(Container()));
                  }),
        ],
      ),
    );
  }
}
