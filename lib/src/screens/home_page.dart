import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/result_page_new.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/get_month_alpha.dart';
import 'package:flightbooking/src/utils/get_week_days_for_result_app_bar.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/home_date_selection.dart';
import 'package:flightbooking/src/widgets/home_drawer.dart';
import 'package:flightbooking/src/widgets/home_page_destination_selection_box.dart';
import 'package:flightbooking/src/widgets/home_page_traveller_selection_box.dart';
import 'package:flightbooking/src/widgets/home_screen_bottom_sheet.dart';
import 'package:flightbooking/src/widgets/home_screen_slide_switch.dart';
import 'package:flightbooking/src/widgets/home_screen_submit_button.dart';
import 'package:flightbooking/src/widgets/passenger_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  String destinationFrom;
  String destinationFromName;
  String destinationTo;
  String destinationToName;
  String departureDate;
  String departureMonth;
  String departureDateNumber;
  String departureDateOneWay;
  String departureMonthOneWay;
  String departureDateNumberOneWay;
  String arrivalDate;
  String arrivalMonth;
  String arrivalDateNumber;
  String arrivalYear;
  String departureYearOneWay;
  String departureYear;
  String classSelected;
  String classForApiCall;
  int classSelect = 0;
  int passengerCount = 0;
  int adultCount = 0;
  int childrenCount = 0;
  int infantCount = 0;
  int listViewCount;
  bool roundTripStatus;
  String roundTrip;
  String tempShortForm;
  String tempName;

  @override
  void initState() {
    destinationToCheck();
    destinationFromCheck();
    checkInDateCheck();
    checkOutDateCheck();
    roundTripStatusCheck();
    classSelectedCheck();
    passengerCountCheck();
    ObjectFactory().hiveBox.putSortByForFilter(value: "price");
    ObjectFactory().hiveBox.putStopsNumber(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime1(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime2(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime3(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime4(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime1(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime2(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime3(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime4(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putAirlineFilterSearch(value: null);
    ObjectFactory().hiveBox.putSelectAllButtonStatus(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeLowerForFilter(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeUpperForFilter(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMin(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMax(value: null);
    ObjectFactory().hiveBox.putDepartureArrayMin(value: null);
    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(statusBarColor: Constants.kitGradients[0]),
      child: Scaffold(
          key: _globalKey,
          drawer: HomeDrawer(),
          resizeToAvoidBottomPadding: false,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: screenHeight(context, dividedBy: 2.5),
                width: screenWidth(context, dividedBy: 1),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Constants.kitGradients[0].withOpacity(0.85),
                      Constants.kitGradients[0].withOpacity(0.9),
                      Constants.kitGradients[0],
                      Constants.kitGradients[0],
                    ],
                    tileMode: TileMode.repeated,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 23),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 23),
                        ),
                        GestureDetector(
                          onTap: () {
                            FirebaseAnalytics().logEvent(
                                name: "clicked_drawer_from_home_screen",
                                parameters: null);
                            _globalKey.currentState.openDrawer();
                          },
                          child: Padding(
                            padding: EdgeInsets.only(left: 4.0),
                            child: Container(
                              width: screenWidth(context, dividedBy: 4),
                              height: screenHeight(context, dividedBy: 20),
                              color: Colors.transparent,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 80),
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 14),
                                    height: screenWidth(context, dividedBy: 90),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 120),
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 20),
                                    height: screenWidth(context, dividedBy: 90),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 150),
                              ),
                              Text(
                                "Search Flights",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'AirbnbCerealRegular',
                                    fontSize: 24,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              DestinationSelectionBox(
                                heading: "From",
                                icon: "assets/icons/departure_icon.svg",
                                destinationShort: destinationFrom,
                                destination: destinationFromName,
                                destinationTo: false,
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    tempShortForm = destinationFrom;
                                    destinationFrom = destinationTo;
                                    destinationTo = tempShortForm;

                                    tempName = destinationFromName;
                                    destinationFromName = destinationToName;
                                    destinationToName = tempName;
                                  });

                                  ObjectFactory().hiveBox.hivePut(
                                      key: 'destinationTo',
                                      value: destinationTo);
                                  ObjectFactory().hiveBox.hivePut(
                                      key: 'destinationToName',
                                      value: destinationToName);

                                  ObjectFactory().hiveBox.hivePut(
                                      key: 'destinationFrom',
                                      value: destinationFrom);
                                  ObjectFactory().hiveBox.hivePut(
                                      key: 'destinationFromName',
                                      value: destinationFromName);
                                },
                                child: Container(
                                  width: screenWidth(context, dividedBy: 4),
                                  color: Colors.transparent,
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width:
                                            screenWidth(context, dividedBy: 20),
                                      ),
                                      Container(
                                        height: screenHeight(context,
                                            dividedBy: 40),
                                        width: 2,
                                        color: Constants.kitGradients[0],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              DestinationSelectionBox(
                                heading: "To",
                                icon: "assets/icons/arrival_icon.svg",
                                destinationShort: destinationTo,
                                destination: destinationToName,
                                destinationTo: true,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 15)),
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    HomeScreenSlideSwitch(
                      roundTripStatus: roundTripStatus,
                      tripValue: (value) {
                        setState(() {
                          roundTripStatus = value;
                          roundTrip = value == false ? "oneway" : "round";
                        });
                      },
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    DateSelectionWidget(
                      roundTripStatus: roundTripStatus,
                      oneWayDepartDate: departureDateOneWay,
                      oneWayDepartMonth: departureMonthOneWay,
                      oneWayDepartYear: departureYearOneWay,
                      departDate: departureDate,
                      departureMonth: departureMonth,
                      departYear: departureYear,
                      arrivalYear: arrivalYear,
                      arrivalDate: arrivalDate,
                      arrivalMonth: arrivalMonth,
                    ),
                    Divider(
                      thickness: 2,
                      color: Theme.of(context).dividerColor,
                    ),
                    TravellerSelectionBox(
                      icon: "assets/icons/person_icon.svg",
                      adultNo: adultCount,
                      childrenNo: childrenCount,
                      infant: infantCount,
                      heading: "TRAVELLER",
                      traveller: true,
                      onTap: () {
                        FirebaseAnalytics().logEvent(
                            name: "opened_bottom_sheet", parameters: null);
                        displayPassengerBottomSheet(context);
                      },
                    ),
                    TravellerSelectionBox(
                      icon: "assets/icons/chair_icon.svg",
                      heading: "CLASS",
                      subHeading: classSelected,
                      onTap: () {
                        FirebaseAnalytics().logEvent(
                            name: "opened_bottom_sheet", parameters: null);
                        displayClassBottomSheet(context);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 28),
              ),
              HomeScreenSubmitButton(
                buttonName: "Search",
                onTap: () async {
                  FirebaseAnalytics().logEvent(
                      name: "clicked_search_button", parameters: null);
                  bool result = await DataConnectionChecker().hasConnection;
                  if (result == true &&
                      ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom') !=
                          null &&
                      ObjectFactory().hiveBox.hiveGet(key: 'destinationTo') !=
                          null) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ResultPageNew(
                                flyFrom: destinationFrom,
                                flyTo: destinationTo,
                                destinationFromName: destinationFromName,
                                destinationToName: destinationToName,
                                routeFrom: destinationFromName,
                                routeTo: destinationToName,
                                dateFrom: roundTripStatus == true
                                    ? departureDateNumber +
                                        "/" +
                                        (ObjectFactory().hiveBox.hiveGet(
                                                    key: 'checkInMonth') !=
                                                null
                                            ? ObjectFactory()
                                                .hiveBox
                                                .hiveGet(key: 'checkInMonth')
                                                .toString()
                                            : DateTime.now().month.toString()) +
                                        "/" +
                                        departureYear
                                    : departureDateNumberOneWay +
                                        "/" +
                                        (ObjectFactory()
                                                    .hiveBox
                                                    .getOneWayMonth() !=
                                                null
                                            ? ObjectFactory()
                                                .hiveBox
                                                .getOneWayMonth()
                                            : DateTime.now().month.toString()) +
                                        "/" +
                                        departureYearOneWay,
                                dateTo: roundTripStatus == true
                                    ? departureDateNumber +
                                        "/" +
                                        (ObjectFactory().hiveBox.hiveGet(
                                                    key: 'checkInMonth') !=
                                                null
                                            ? ObjectFactory()
                                                .hiveBox
                                                .hiveGet(key: 'checkInMonth')
                                                .toString()
                                            : DateTime.now().month.toString()) +
                                        "/" +
                                        departureYear
                                    : departureDateNumberOneWay +
                                        "/" +
                                        (ObjectFactory()
                                                    .hiveBox
                                                    .getOneWayMonth() !=
                                                null
                                            ? ObjectFactory()
                                                .hiveBox
                                                .getOneWayMonth()
                                                .toString()
                                            : DateTime.now().month.toString()) +
                                        "/" +
                                        departureYearOneWay,
                                flightType: roundTrip,
                                adults: adultCount,
                                children: childrenCount,
                                infants: infantCount,
                                selectedCabins: classForApiCall,
                                returnFrom: arrivalDateNumber +
                                    "/" +
                                    (ObjectFactory().hiveBox.hiveGet(
                                                key: 'checkOutMonth') !=
                                            null
                                        ? ObjectFactory()
                                            .hiveBox
                                            .hiveGet(key: 'checkOutMonth')
                                            .toString()
                                        : DateTime.now().month.toString()) +
                                    "/" +
                                    arrivalYear,
                                returnTo: arrivalDateNumber +
                                    "/" +
                                    (ObjectFactory().hiveBox.hiveGet(
                                                key: 'checkOutMonth') !=
                                            null
                                        ? ObjectFactory()
                                            .hiveBox
                                            .hiveGet(key: 'checkOutMonth')
                                            .toString()
                                        : DateTime.now().month.toString()) +
                                    "/" +
                                    arrivalYear,
                                passengerCount: passengerCount.toString(),
                                roundTripStatus: roundTripStatus,
                                departureDate: roundTripStatus == true
                                    ? departureDate
                                    : departureDateOneWay,
                                dateTimePassengersDayInAlphabet: getTranslated(
                                    context,
                                    roundTripStatus == true
                                        ? (ObjectFactory().hiveBox.hiveGet(
                                                        key:
                                                            'checkInDateWeekDay') !=
                                                    null
                                                ? GetDayForResultAppBar()
                                                    .getWeekDayForResultAppBar(
                                                        ObjectFactory()
                                                            .hiveBox
                                                            .hiveGet(
                                                                key:
                                                                    'checkInDateWeekDay')
                                                            .toString())
                                                    .toString()
                                                : GetDayForResultAppBar()
                                                    .getWeekDayForResultAppBar(
                                                        DateTime.now()
                                                            .weekday
                                                            .toString()))
                                            .toString()
                                        : (ObjectFactory().hiveBox.getOneWayDate() !=
                                                    null
                                                ? GetDayForResultAppBar()
                                                    .getWeekDayForResultAppBar(
                                                        ObjectFactory()
                                                            .hiveBox
                                                            .getOneWayDate()
                                                            .toString())
                                                    .toString()
                                                : GetDayForResultAppBar()
                                                    .getWeekDayForResultAppBar(
                                                        DateTime.now().weekday.toString()))
                                            .toString()),
                                dateTimePassengersDayReturnInAlphabet: getTranslated(
                                    context,
                                    (ObjectFactory().hiveBox.hiveGet(
                                                    key:
                                                        'checkOutDateWeekDay') !=
                                                null
                                            ? GetDayForResultAppBar()
                                                .getWeekDayForResultAppBar(
                                                    ObjectFactory()
                                                        .hiveBox
                                                        .hiveGet(
                                                            key:
                                                                'checkOutDateWeekDay')
                                                        .toString())
                                                .toString()
                                            : GetDayForResultAppBar()
                                                .getWeekDayForResultAppBar(
                                                    DateTime.now()
                                                        .add(Duration(days: 1))
                                                        .weekday
                                                        .toString()))
                                        .toString()),
                                dateTimePassengers: roundTripStatus == true
                                    ? departureDate.toString() +
                                        " " +
                                        departureMonth
                                    : departureDateOneWay.toString() +
                                        " " +
                                        departureMonthOneWay,
                                dateTimeReturnPassengers:
                                    arrivalDate.toString() + " " + arrivalMonth,
                                departureCode: destinationFrom,
                                destinationCode: destinationTo,
                              )),
                    );
                  } else {
                    result == false
                        ? FirebaseAnalytics().logEvent(
                            name: "no_internet_connection", parameters: null)
                        : FirebaseAnalytics().logEvent(
                            name: "empty_departure_arrival_fields",
                            parameters: null);
                    showToastLong(result == false
                        ? getTranslated(
                            context, "Check_your_Internet_Connection")
                        : getTranslated(
                            context, "Please_choose_departure_and_arrival"));
                  }
                },
              )
            ],
          )),
    );
  }

  void checkInDateCheck() {
    if (ObjectFactory().hiveBox.getOneWayDay() != null &&
        (DateTime.parse(ObjectFactory().hiveBox.getOneWayYear() +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.getOneWayMonth()) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.getOneWayDay()) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        departureDateOneWay = ObjectFactory().hiveBox.getOneWayDay();
        departureMonthOneWay = GetMonthName()
            .getMonthAlpha(ObjectFactory().hiveBox.getOneWayMonth());
        departureYearOneWay = ObjectFactory().hiveBox.getOneWayYear();
        departureDateNumberOneWay = ObjectFactory().hiveBox.getOneWayDay();
      });
    } else {
      departureDateOneWay = DateTime.now().day.toString();
      departureMonthOneWay =
          GetMonthName().getMonthAlpha(DateTime.now().month.toString());
      departureYearOneWay = DateTime.now().year.toString();
      departureDateNumberOneWay = DateTime.now().day.toString();
    }
    if (ObjectFactory().hiveBox.hiveGet(key: 'checkInDate') != null &&
        (DateTime.parse(ObjectFactory().hiveBox.hiveGet(key: 'checkInYear') +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth')) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkInDate')) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        departureDate = ObjectFactory().hiveBox.hiveGet(key: 'checkInDate');

        departureMonth = GetMonthName().getMonthAlpha(
            ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth'));

        departureYear = ObjectFactory().hiveBox.hiveGet(key: 'checkInYear');

        departureDateNumber =
            ObjectFactory().hiveBox.hiveGet(key: 'checkInDate');
      });
    } else {
      setState(() {
        departureDate = DateTime.now().day.toString();
        departureMonth =
            GetMonthName().getMonthAlpha(DateTime.now().month.toString());
        departureYear = DateTime.now().year.toString();
        departureDateNumber = DateTime.now().day.toString();
      });
    }
    //print("departure date is "+departureDateNumber.toString()+"/"+ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth').toString()+"/"+departureYear.toString());
  }

  void checkOutDateCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate') != null &&
        (DateTime.parse(ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear') +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth')) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate')) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        arrivalDate = ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
        arrivalMonth = GetMonthName()
            .getMonthAlpha(
                ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth'))
            .toString();
        arrivalYear = ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear');
        arrivalDateNumber =
            ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
      });
      if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') == "true") {
        setState(() {
          arrivalDate = ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
          arrivalMonth = GetMonthName()
              .getMonthAlpha(
                  ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth'))
              .toString();
          arrivalYear = ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear');
          arrivalDateNumber =
              ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
        });
      }
    } else {
      setState(() {
        arrivalDate = DateTime.now().add(Duration(days: 1)).day.toString();
        arrivalMonth =
            GetMonthName().getMonthAlpha(DateTime.now().month.toString());
        arrivalYear = DateTime.now().year.toString();
        arrivalDateNumber =
            DateTime.now().add(Duration(days: 1)).day.toString();
      });
    }
  }

  void roundTripStatusCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') != null) {
      if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') == "true") {
        setState(() {
          roundTripStatus = true;
        });
      } else if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') ==
          "false") {
        setState(() {
          roundTripStatus = false;
        });
      }
    } else {
      setState(() {
        roundTripStatus = true;
        ObjectFactory().hiveBox.hivePut(key: 'roundTripStatus', value: "true");
      });
    }
    roundTripStatus == true
        ? setState(() {
            roundTrip = "round";
          })
        : setState(() {
            roundTrip = "oneway";
          });
  }

  void destinationFromCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom') != null) {
      setState(() {
        destinationFrom =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom');
        destinationFromName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFromName');
      });
    } else {
      ObjectFactory().hiveBox.hivePut(key: 'destinationFrom', value: "NYC");
      ObjectFactory()
          .hiveBox
          .hivePut(key: 'destinationFromName', value: "New York");
      setState(() {
        destinationFrom =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom');
        destinationFromName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFromName');
      });
    }
  }

  void destinationToCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'destinationTo') != null) {
      setState(() {
        destinationTo = ObjectFactory().hiveBox.hiveGet(key: 'destinationTo');
        destinationToName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationToName');
      });
    } else {
      ObjectFactory().hiveBox.hivePut(key: 'destinationTo', value: "LON");
      ObjectFactory()
          .hiveBox
          .hivePut(key: 'destinationToName', value: "London");

      setState(() {
        destinationTo = ObjectFactory().hiveBox.hiveGet(key: 'destinationTo');
        destinationToName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationToName');
      });
    }
  }

  void displayClassBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return HomeBottomSheet();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
    );
  }

  void displayPassengerBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return PassengerBottomSheet();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
    );
  }

  void passengerCountCheck() {
    ObjectFactory().hiveBox.hiveGetInt(key: "adultCount") != null
        ? setState(() {
            adultCount = ObjectFactory().hiveBox.hiveGetInt(key: "adultCount");
          })
        : setState(() {
            adultCount = 1;
          });
    ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount") != null
        ? setState(() {
            childrenCount =
                ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount");
          })
        : setState(() {
            childrenCount = 0;
          });
    ObjectFactory().hiveBox.hiveGetInt(key: "infantCount") != null
        ? setState(() {
            infantCount =
                ObjectFactory().hiveBox.hiveGetInt(key: "infantCount");
          })
        : setState(() {
            infantCount = 0;
          });

    int sum = adultCount + childrenCount + infantCount;
    setState(() {
      passengerCount = sum;
    });
    ObjectFactory()
        .hiveBox
        .hivePutInt(key: "totalPassengerCount", value: passengerCount);
    print('Total passenger count is ' +
        ObjectFactory()
            .hiveBox
            .hiveGetInt(key: "totalPassengerCount")
            .toString());
  }

  void classSelectedCheck() {
    if ((ObjectFactory().hiveBox.hiveGetBool(key: "economy") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "premiumEconomy") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "business") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "firstClass") == null)) {
      classSelect = 1;
      classSelected = "Economy";
      classForApiCall = "M";
    }

    if (ObjectFactory().hiveBox.hiveGetBool(key: "economy") == true) {
      setState(() {
        classSelect = 1;
        classSelected = "Economy";
        classForApiCall = "M";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "premiumEconomy") ==
        true) {
      setState(() {
        classSelect = 2;
        classSelected = "Premium Economy";
        classForApiCall = "W";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "business") == true) {
      setState(() {
        classSelect = 3;
        classSelected = "Business";
        classForApiCall = "C";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "firstClass") == true) {
      setState(() {
        classSelect = 4;
        classSelected = "First class";
        classForApiCall = "F";
      });
    }
  }
}
