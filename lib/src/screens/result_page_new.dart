import 'dart:math';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/bloc/app_bloc.dart';
import 'package:flightbooking/src/models/searchFlightRequest.dart';
import 'package:flightbooking/src/models/search_response.dart';
import 'package:flightbooking/src/screens/home_page.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_appbar_widget.dart';
import 'package:flightbooking/src/widgets/result_list_top.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet.dart';
import 'package:flightbooking/src/widgets/result_shrimmer.dart';
import 'package:flightbooking/src/widgets/result_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import '../utils/object_factory.dart';
import 'details.dart';

class ResultPageNew extends StatefulWidget {
  bool roundTripStatus;
  String routeFrom,
      routeTo,
      dateTimePassengers,
      dateTimeReturnPassengers,
      dateTimePassengersDayInAlphabet,
      dateTimePassengersDayReturnInAlphabet,
      destinationFromName,
      destinationToName,
      departureDate,
      passengerCount,
      flyFrom,
      flyTo,
      dateFrom,
      dateTo,
      flightType,
      selectedCabins,
      returnFrom,
      returnTo,
      destinationCode,
      departureCode;
  int adults, children, infants, departureStops = 0, returnStops = 0;

  ResultPageNew({
    this.selectedCabins,
    this.infants,
    this.children,
    this.adults,
    this.returnTo,
    this.returnFrom,
    this.flightType,
    this.dateTo,
    this.dateFrom,
    this.flyFrom,
    this.flyTo,
    this.roundTripStatus,
    this.routeFrom,
    this.routeTo,
    this.destinationFromName,
    this.destinationToName,
    this.passengerCount,
    this.departureDate,
    this.destinationCode,
    this.departureCode,
    this.dateTimePassengers,
    this.dateTimeReturnPassengers,
    this.dateTimePassengersDayInAlphabet,
    this.dateTimePassengersDayReturnInAlphabet,
  });

  @override
  _ResultPageNewState createState() => _ResultPageNewState();
}

class _ResultPageNewState extends State<ResultPageNew> {
  ScrollController _controller = ScrollController();
  AppBloc appBloc = new AppBloc();
  bool loadingShimmer = true;
  int listViewCount;
  String price;
  int switchingStatus;
  List<int> returnStopsArray = [];
  List<int> departStopsArray = [];
  List<String> departTimeArray = [];
  List<DateTime> departTimeArrayInDateTime = [];
  List<String> departTimeArrayReturn = [];
  List<String> arrivalTimeArray = [];
  List<DateTime> arrivalTimeArrayInDateTime = [];
  List<String> arrivalTimeArrayReturn = [];
  List<String> airlineReturnArray = [];
  List<String> airlineDepartureArray = [];
  List<int> durationDepartureArray = [];
  List<int> durationReturnArray = [];
  List<int> durationTotalArray = [];
  List<String> filterParametersUi = [
    "Cheapest",
    "Stops",
    "Flight duration",
    "Flight times",
    "Airlines",
    "Price",
  ];
  List<bool> selectedIndex = [true, false, false, false, false, false];
  List<int> priceTotalArray = [];
  List<List<dynamic>> resultPageAirlineArray = [];
  Color filterTextColor, filterContainerColor;
  SearchResponse searchResponse;

  bool tickCheapest, tickQuickest;
  double departRangeValue, returnRangeValue;

  bool departureFlightTime1,
      departureFlightTime2,
      departureFlightTime3,
      departureFlightTime4,
      returnFlightTime1,
      returnFlightTime2,
      returnFlightTime3,
      returnFlightTime4;
  double x;
  double y;
  bool status;
  double flightDuration;
  int flightDurationInHours, stops;
  String departureTimeFrom = '';
  String departureTimeTo = '';
  String returnTimeFrom = '';
  String returnTimeTo = '';
  int priceRangeLowerValue, priceRangeUpperValue;

  bool showFloatingButton, showHorizontalFilterListView;

  bool direct, tickOneStop, tickTwoPlusStops;

  @override
  void initState() {
    setState(() {
      tickCheapest = true;
      tickQuickest = false;
      switchingStatus = 0;
      showFloatingButton = false;
      showHorizontalFilterListView = false;
      direct = true;
      tickOneStop = true;
      tickTwoPlusStops = true;
    });
    appBloc.flightSearch.listen((event) async {
      getDataInArray(event);
      event.data.length == 0
          ? setState(() {
              switchingStatus = 2;
            })
          : setState(() {
              switchingStatus = 1;
              showHorizontalFilterListView = true;
            });
    });
    searchAPICallForFilter();
    print("trip" + widget.roundTripStatus.toString());
    super.initState();
  }

  @override
  void dispose() {
    clearData();
    appBloc.dispose();
    super.dispose();
  }

  Future<void> _backwardApp(BuildContext context) async {
    ObjectFactory().hiveBox.putStopsNumber(value: null);
    ObjectFactory().hiveBox.putFlightDuration(value: null);
    ObjectFactory().hiveBox.putDepartureFromTime(value: null);
    ObjectFactory().hiveBox.putReturnFromTime(value: null);
    ObjectFactory().hiveBox.putDepartureToTime(value: null);
    ObjectFactory().hiveBox.putReturnToTime(value: null);
    ObjectFactory().hiveBox.putSortByForFilter(value: "price");
    ObjectFactory().hiveBox.putStopsNumber(value: null);

    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putAirlineFilterSearch(value: null);
    ObjectFactory().hiveBox.putSelectAllButtonStatus(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeLowerForFilter(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeUpperForFilter(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMin(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMax(value: null);
    ObjectFactory().hiveBox.putDepartureArrayMin(value: null);
    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => HomePage()), (route) => false);
  }

  Future clearData() async {
    ObjectFactory().hiveBox.putStopsNumber(value: null);
    ObjectFactory().hiveBox.putFlightDuration(value: null);
    ObjectFactory().hiveBox.putDepartureFromTime(value: null);
    ObjectFactory().hiveBox.putReturnFromTime(value: null);
    ObjectFactory().hiveBox.putDepartureToTime(value: null);
    ObjectFactory().hiveBox.putReturnToTime(value: null);
    ObjectFactory().hiveBox.putSortByForFilter(value: "price");
    ObjectFactory().hiveBox.putStopsNumber(value: null);

    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putAirlineFilterSearch(value: null);
    ObjectFactory().hiveBox.putSelectAllButtonStatus(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeLowerForFilter(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeUpperForFilter(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMin(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMax(value: null);
    ObjectFactory().hiveBox.putDepartureArrayMin(value: null);

    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    // returnStopsArray.clear();
    // departStopsArray.clear();
    // departTimeArray.clear();
    // departTimeArrayInDateTime.clear();
    // departTimeArrayReturn.clear();
    // arrivalTimeArray.clear();
    // arrivalTimeArrayInDateTime.clear();
    // arrivalTimeArrayReturn.clear();
    // airlineReturnArray.clear();
    // airlineDepartureArray.clear();
    // durationDepartureArray.clear();
    // durationReturnArray.clear();
    // priceTotalArray.clear();
    // resultPageAirlineArray.clear();
    // await ObjectFactory().hiveBox.putSortByForFilter(value: null);
    // await ObjectFactory().hiveBox.putSortByForStopsDirect(value: null);
    // await ObjectFactory().hiveBox.putSortByForStopsOne(value: null);
    // await ObjectFactory().hiveBox.putSortByForStopsTwo(value: null);
    // await ObjectFactory().hiveBox.putDepartureRangeStart(value: null);
    // await ObjectFactory().hiveBox.putReturnRangeEnd(value: null);
    // await ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    // await ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    // await ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    // await ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    // await ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    // await ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    // await ObjectFactory().hiveBox.putReturnFlightTimeFrom(value: null);
    // await ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    // await ObjectFactory().hiveBox.putAirlineFilterSearch(value: null);
    // await ObjectFactory().hiveBox.putSelectAllButtonStatus(value: null);
    // await ObjectFactory()
    //     .hiveBox
    //     .putSelectedPriceRangeLowerForFilter(value: null);
    // await ObjectFactory()
    //     .hiveBox
    //     .putSelectedPriceRangeUpperForFilter(value: null);
    // await ObjectFactory().hiveBox.putPriceTotalArrayMin(value: null);
    // await ObjectFactory().hiveBox.putPriceTotalArrayMax(value: null);
    // await ObjectFactory().hiveBox.putDepartureArrayMin(value: null);
    // await ObjectFactory().hiveBox.putDepartureArrayMax(value: null);
    // await ObjectFactory().hiveBox.putReturnArrayMax(value: null);
    // await ObjectFactory().hiveBox.putReturnArrayMax(value: null);
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "result_page", parameters: null);
    return WillPopScope(
      onWillPop: () => _backwardApp(context),
      child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(
            statusBarColor: Constants.kitGradients[14],
          ),
          child: Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            floatingActionButton: showFloatingButton == true
                ? Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 3.4),
                        ),
                        GestureDetector(
                          child: Container(
                            width: screenWidth(context, dividedBy: 2),
                            height: screenHeight(context, dividedBy: 15),
                            decoration: BoxDecoration(
                                color: Constants.kitGradients[3],
                                borderRadius: BorderRadius.circular(25.0),
                                border: Border.all(
                                    color: Constants.kitGradients[2],
                                    width: 1.0)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.refresh_rounded,
                                  color: Constants.kitGradients[14],
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 100),
                                ),
                                Text('reset filter',
                                    style: TextStyle(
                                      fontFamily: 'ProximaNova',
                                      color: Constants.kitGradients[2],
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.normal,
                                    ))
                              ],
                            ),
                          ),
                          onTap: () {
                            selectedIndex.clear();
                            setState(() {
                              priceRangeUpperValue = null;
                              priceRangeLowerValue = null;
                              stops = 5;
                              flightDurationInHours = null;
                              departureTimeTo = '';
                              departureTimeFrom = '';
                              returnTimeTo = '';
                              returnTimeFrom = '';
                              showFloatingButton = false;
                              selectedIndex = [
                                true,
                                false,
                                false,
                                false,
                                false,
                                false
                              ];
                            });

                            ObjectFactory().hiveBox.putStopsNumber(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putFlightDuration(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureFromTime(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnFromTime(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureToTime(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnToTime(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putSortByForFilter(value: "price");
                            ObjectFactory().hiveBox.putStopsNumber(value: null);

                            ObjectFactory()
                                .hiveBox
                                .putDepartureFlightTimeFrom(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureFlightTimeTo(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnFlightTimeTo(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnFlightTimeTo(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureFlightTimeFrom(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureFlightTimeTo(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnFlightTimeFrom(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnFlightTimeTo(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putAirlineFilterSearch(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putSelectAllButtonStatus(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putSelectedPriceRangeLowerForFilter(
                                    value: null);
                            ObjectFactory()
                                .hiveBox
                                .putSelectedPriceRangeUpperForFilter(
                                    value: null);
                            ObjectFactory()
                                .hiveBox
                                .putPriceTotalArrayMin(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putPriceTotalArrayMax(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putDepartureArrayMin(value: null);

                            ObjectFactory()
                                .hiveBox
                                .putReturnArrayMax(value: null);
                            ObjectFactory()
                                .hiveBox
                                .putReturnArrayMax(value: null);
                            searchAPICallForFilter();
                          },
                        )
                      ],
                    ),
                  )
                : Container(),
            appBar: AppBar(
              backgroundColor: Constants.kitGradients[3],
              leading: Container(),
              toolbarHeight: screenHeight(context, dividedBy: 8),
              actions: [
                ResultAppBarWidget(
                  routeFrom: widget.routeFrom,
                  routeTo: widget.routeTo,
                  dateTimePassengers: widget.roundTripStatus == true
                      ? (widget.dateTimePassengersDayInAlphabet +
                          " " +
                          widget.dateTimePassengers +
                          " - " +
                          widget.dateTimePassengersDayReturnInAlphabet +
                          " " +
                          widget.dateTimeReturnPassengers +
                          ", " +
                          widget.passengerCount +
                          " " +
                          (widget.passengerCount == "1"
                              ? "passenger"
                              : "passenegers"))
                      : (widget.dateTimePassengersDayInAlphabet +
                          " " +
                          widget.dateTimePassengers +
                          ", " +
                          widget.passengerCount +
                          " " +
                          (widget.passengerCount == "1"
                              ? "passenger"
                              : "passenegers")),
                ),
              ],
            ),
            body: Column(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: 0.3,
                  color: Constants.kitGradients[4],
                ),
                Container(
                  height: screenHeight(context, dividedBy: 17),
                  width: screenWidth(context, dividedBy: 1),
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Center(
                    child: showHorizontalFilterListView == true
                        ? ListView.builder(
                            controller: _controller,
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: 6,
                            itemBuilder: (BuildContext context, index) {
                              bool isCurrentIndexSelected =
                                  selectedIndex[index] == true;
                              if (isCurrentIndexSelected) {
                                filterContainerColor =
                                    Constants.kitGradients[0];
                                filterTextColor = Colors.white;
                              } else {
                                filterContainerColor =
                                    Constants.kitGradients[10];
                                filterTextColor = Constants.kitGradients[4];
                              }
                              return ResultListTop(
                                title: filterParametersUi[index],
                                containerColor: filterContainerColor,
                                textColor: filterTextColor,
                                itemIndex: index,
                                onTap: () {
                                  displayBottomSheet(context, index);
                                  FirebaseAnalytics().logEvent(
                                      name: "result_filter_applied",
                                      parameters: {
                                        "sort_by":
                                            filterParametersUi[index].toString()
                                      });
                                },
                              );
                            })
                        : ListView.builder(
                            controller: _controller,
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: 6,
                            itemBuilder: (BuildContext context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Shimmer.fromColors(
                                  baseColor: Theme.of(context).primaryColorDark,
                                  highlightColor: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.5),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 5),
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                  ),
                                ),
                              );
                            }),
                  ),
                ),
                Expanded(
                  child: StreamBuilder<SearchResponse>(
                      stream: appBloc.flightSearch,
                      builder: (context, snapshot) {
                        // print("count" + snapshot.data.data.length.toString());
                        return switchingStatus == 1
                            ? ListView.builder(
                                controller: _controller,
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  print("deepLink");
                                  print(snapshot.data.data[index].deepLink);
                                  return ResultsPageTile(
                                    roundTripStatus: widget.roundTripStatus,
                                    offersFound: '1',
                                    price: snapshot.data.data[index].price
                                        .toString(),
                                    oneWayTotalTime: getDurationInHour(snapshot
                                        .data.data[index].duration.departure),
                                    roundTripTotalTime:
                                        widget.roundTripStatus == true
                                            ? getDurationInHour(snapshot
                                                .data
                                                .data[index]
                                                .duration
                                                .durationReturn)
                                            : "",
                                    oneWayStopsNumber:
                                        getStops(index, departStopsArray)
                                            .toString(),
                                    roundTripStopsNumber:
                                        widget.roundTripStatus == true
                                            ? getStops(index, returnStopsArray)
                                                .toString()
                                            : 0.toString(),
                                    oneWayDepartureTime:
                                        getTime(departTimeArray[index]),
                                    oneWayArrivalTime:
                                        getTime(arrivalTimeArray[index]),
                                    roundTripDepartureTime: widget
                                                .roundTripStatus ==
                                            true
                                        ? getTime(departTimeArrayReturn[index])
                                        : "",
                                    roundTripArrivalTime: widget
                                                .roundTripStatus ==
                                            true
                                        ? getTime(arrivalTimeArrayReturn[index])
                                        : "",
                                    oneWayDepartureAirportName:
                                        snapshot.data.data[index].flyFrom,
                                    oneWayArrivalAirportName:
                                        snapshot.data.data[index].flyTo,
                                    roundTripDepartureAirportName:
                                        snapshot.data.data[index].flyTo,
                                    roundTripAirportName:
                                        snapshot.data.data[index].flyFrom,
                                    resultPageAirlineArrayByIndex:
                                        resultPageAirlineArray[index],
                                    onTap: () async {
                                      FirebaseAnalytics().logEvent(
                                          name: "clicked_result_item",
                                          parameters: {
                                            "selected_item_index":
                                                index.toString()
                                          });
                                      bool result =
                                          await DataConnectionChecker()
                                              .hasConnection;
                                      if (result == true) {
                                        push(
                                            context,
                                            DetailsPage(
                                              roundTripStatus:
                                                  widget.roundTripStatus,
                                              searchResponse: snapshot.data,
                                              dataIndex: index,
                                              departStop: getStops(
                                                  index, departStopsArray),
                                              returnStop:
                                                  widget.roundTripStatus == true
                                                      ? getStops(index,
                                                          returnStopsArray)
                                                      : 0,
                                            ));
                                      } else {
                                        FirebaseAnalytics().logEvent(
                                            name: "no_internet_connection",
                                            parameters: null);
                                        showToastLong(getTranslated(context,
                                            "Please_check_your_internet_connection"));
                                      }
                                    },
                                  );
                                })
                            : switchingStatus == 0
                                ? ResultShrimmer(
                                    roundTripStatus: widget.roundTripStatus,
                                    route: widget.destinationFromName
                                            .split(" ")
                                            .first +
                                        " - " +
                                        widget.destinationToName
                                            .split(" ")
                                            .first,
                                    dateTimePassengers: (ObjectFactory()
                                                        .hiveBox
                                                        .hiveGet(
                                                            key:
                                                                'checkInDateWeekDay') !=
                                                    null
                                                ? ObjectFactory()
                                                    .getDay
                                                    .getWeekDay(ObjectFactory()
                                                        .hiveBox
                                                        .hiveGet(
                                                            key:
                                                                'checkInDateWeekDay')
                                                        .toString())
                                                    .toString()
                                                : ObjectFactory()
                                                    .getDay
                                                    .getWeekDay(DateTime.now()
                                                        .weekday
                                                        .toString()))
                                            .toString() +
                                        " " +
                                        widget.departureDate.toString() +
                                        ", " +
                                        widget.passengerCount.toString() +
                                        " " +
                                        "passengers")
                                : switchingStatus == 2
                                    ? Center(
                                        child: Text(
                                          getTranslated(
                                              context, "No flights found"),
                                          style: TextStyle(
                                            fontFamily: 'ProximaNova',
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                      )
                                    : Container();
                      }),
                ),
              ],
            ),
          )),
    );
  }

  void getDataInArray(event) {
    departStopsArray.clear();
    returnStopsArray.clear();
    departTimeArray.clear();
    arrivalTimeArray.clear();
    departTimeArrayReturn.clear();
    arrivalTimeArrayReturn.clear();
    airlineDepartureArray.clear();
    airlineDepartureArray.clear();
    airlineReturnArray.clear();
    durationDepartureArray.clear();
    durationReturnArray.clear();
    durationTotalArray.clear();
    departTimeArrayInDateTime.clear();
    arrivalTimeArrayInDateTime.clear();
    priceTotalArray.clear();
    resultPageAirlineArray.clear();

    setState(() {
      widget.departureStops = 0;
      widget.returnStops = 0;
    });

    for (int i = 0; i < event.data.length; i++) {
      for (int x = 0; x < event.data[i].route.length; x++) {
        if ((event.data[i].route[x].routeReturn == 0) &&
            (event.data[i].route[x].cityCodeTo.toString() ==
                event.data[i].cityCodeTo.toString())) {
          // arrivalPortArray.add(event.data[i].route[x].flyTo.toString());
          setState(() {
            arrivalTimeArray
                .add(event.data[i].route[x].localArrival.toString());
          });
        }
        if ((event.data[i].route[x].routeReturn == 1) &&
            (event.data[i].route[x].cityCodeFrom.toString() ==
                event.data[i].cityCodeTo.toString())) {
          setState(() {
            departTimeArrayReturn
                .add(event.data[i].route[x].localDeparture.toString());
            arrivalTimeArrayInDateTime
                .add(event.data[i].route[x].localDeparture);
            // departPortArrayReturn.add(event.data[i].route[x].flyFrom.toString());
          });
        }
      }
    }

    for (int i = 0; i < event.data.length; i++) {
      for (int x = 0; x < event.data[i].route.length; x++) {
        if ((event.data[i].route[x].routeReturn == 0) &&
            (event.data[i].route[x].cityCodeFrom.toString() ==
                event.data[i].cityCodeFrom.toString())) {
          setState(() {
            departTimeArray
                .add(event.data[i].route[x].localDeparture.toString());
            departTimeArrayInDateTime
                .add(event.data[i].route[x].localDeparture);
            // departPortArray.add(event.data[i].route[x].flyFrom.toString());
            airlineDepartureArray
                .add(event.data[i].route[x].airline.toString());
          });
        }

        if ((event.data[i].route[x].routeReturn == 1) &&
            (event.data[i].route[x].cityCodeTo.toString() ==
                event.data[i].cityCodeFrom.toString())) {
          setState(() {
            arrivalTimeArrayReturn
                .add(event.data[i].route[x].localArrival.toString());
            // arrivalPortArrayReturn.add(event.data[i].route[x].flyTo.toString());
            airlineReturnArray.add(event.data[i].route[x].airline.toString());
          });
        }
      }
    }

    for (int i = 0; i < event.data.length; i++) {
      for (int x = 0; x < event.data[i].route.length; x++) {
        if (event.data[i].route[x].routeReturn == 0) {
          setState(() {
            widget.departureStops = widget.departureStops + 1;
          });
        } else if (event.data[i].route[x].routeReturn == 1) {
          setState(() {
            widget.returnStops = widget.returnStops + 1;
          });
        }
      }
      setState(() {
        returnStopsArray.add(widget.returnStops);
        departStopsArray.add(widget.departureStops);
      });
    }
    // print("depart stop array is "+departStopsArray.toString());
    // print("return stop array is "+returnStopsArray.toString());
    // print("depart stop  is "+widget.departureStops.toString());
    // print("return stop  is "+widget.returnStops.toString());

    for (int i = 0; i < event.data.length; i++) {
      setState(() {
        durationDepartureArray.add(event.data[i].duration.departure);
        durationReturnArray.add(event.data[i].duration.durationReturn);
        durationTotalArray.add(event.data[i].duration.total);
        priceTotalArray.add(event.data[i].price);
        resultPageAirlineArray.add(event.data[i].airlines);
      });
    }

    if (event.data.length > 0) {
      ObjectFactory()
          .hiveBox
          .putPriceTotalArrayMin(value: priceTotalArray.reduce(min));
      ObjectFactory()
          .hiveBox
          .putPriceTotalArrayMax(value: priceTotalArray.reduce(max));
      ObjectFactory()
          .hiveBox
          .putDepartureArrayMin(value: durationDepartureArray.reduce(min));

      ObjectFactory()
          .hiveBox
          .putReturnArrayMax(value: durationReturnArray.reduce(max));
      ObjectFactory()
          .hiveBox
          .putReturnArrayMax(value: durationReturnArray.reduce(max));
    }

    // print("priceTotalArray is "+priceTotalArray.toString());
    // print("departTimeArrayInDateTime is  " +
    //     departTimeArrayInDateTime.toString());
    // print("arrivalTimeArrayInDateTime is  " +
    //     arrivalTimeArrayInDateTime.toString());
    // print("durationDepartureArray is  " + durationDepartureArray.toString());
    // print("durationDepartureArrayMax is  " + durationDepartureArray.reduce(max).toString());
    // print("durationReturnArray is  " + durationReturnArray.toString());
    // print("durationReturnArrayMax is  " + durationReturnArray.reduce(max).toString());
    //  print("departTimeArray is  " + departTimeArray.toString());
    //  print("arrivalTimeArray is  " + arrivalTimeArray.toString());
    //  print("departTimeArrayReturn is " + departTimeArrayReturn.toString());
    //  print("arrivalTimeArrayReturn is  " + arrivalTimeArrayReturn.toString());
    // print("departPortArray is  " + departPortArray.toString());
    // print("arrivalPortArray is  " + arrivalPortArray.toString());
    // print("departPortArrayReturn is " + departPortArrayReturn.toString());
    // print("arrivalPortArrayReturn is  " + arrivalPortArrayReturn.toString());
    // print("AIRLINE DEPARTURE array is  " + airlineDepartureArray.toString());
    // print("AIRLINE return array is  " + airlineReturnArray.toString());
    //print("AIRLINE Resultpage array is  " + resultPageAirlineArray.toString());
  }

  void displayBottomSheet(context, index) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext buildContext) {
        return StatefulBuilder(
            builder: (BuildContext bc, StateSetter setState) {
          return index == 0
              ? Wrap(children: [
                  ResultPageBottomSheet(
                    index: index,
                    onTapCheapest: () async {
                      setState(() {
                        showFloatingButton = true;
                        selectedIndex[index] = true;
                      });
                      setState(() {
                        tickCheapest = true;
                        tickQuickest = false;
                      });
                      ObjectFactory()
                          .hiveBox
                          .putSortByForFilter(value: "price");
                      // await clearData();
                      searchAPICallForFilter();
                      Navigator.pop(context);
                    },
                    onTapQuickest: () async {
                      setState(() {
                        selectedIndex[index] = true;
                      });
                      setState(() {
                        showFloatingButton = true;
                      });
                      setState(() {
                        tickCheapest = false;
                        tickQuickest = true;
                      });
                      ObjectFactory()
                          .hiveBox
                          .putSortByForFilter(value: "duration");
                      // await clearData();
                      searchAPICallForFilter();
                      Navigator.pop(context);
                    },
                    tickCheapest: tickCheapest,
                    tickQuickest: tickQuickest,
                  )
                ])
              : index == 1
                  ? Wrap(children: [
                      ResultPageBottomSheet(
                        index: index,
                        // direct: (value) {
                        //   setState(() {
                        //     direct = value;
                        //   });
                        // },
                        // tickOneStop: (value) {
                        //   setState(() {
                        //     tickOneStop = value;
                        //   });
                        // },
                        // tickTwoPlusStops: (value) {
                        //   setState(() {
                        //     tickTwoPlusStops = value;
                        //   });
                        // },
                        stops: (value) {
                          setState(() {
                            stops = int.parse(value.toStringAsFixed(0));
                            ObjectFactory().hiveBox.putStopsNumber(
                                value: value.toStringAsFixed(0));
                          });
                        },
                        onTapSaveButton: () async {
                          // ObjectFactory().hiveBox.putSortByForStopsDirect(
                          //     value: direct.toString().trim());
                          // ObjectFactory().hiveBox.putSortByForStopsOne(
                          //     value: tickOneStop.toString().trim());
                          // ObjectFactory().hiveBox.putSortByForStopsTwo(
                          //     value: tickTwoPlusStops.toString().trim());
                          // selectedIndex.clear();
                          setState(() {
                            showFloatingButton = true;
                            selectedIndex[index] = true;
                          });
                          // await clearData();
                          searchAPICallForFilter();
                          Navigator.pop(context);
                        },
                      )
                    ])
                  : index == 2
                      ? Wrap(children: [
                          ResultPageBottomSheet(
                            index: index,
                            durationDepartureArray: durationDepartureArray,
                            durationReturnArray: durationTotalArray,
                            onTapSaveButton: () async {
                              setState(() {
                                showFloatingButton = true;
                                selectedIndex[index] = true;
                              });
                              // await clearData();
                              searchAPICallForFilter();

                              ObjectFactory().hiveBox.putFlightDuration(
                                  value: returnRangeValue.toStringAsFixed(0));
                              Navigator.pop(context);
                            },
                            departRangeValue: (value) {
                              setState(() {
                                departRangeValue = value;
                                print("departRangevalue" +
                                    departRangeValue.toString());
                              });
                            },
                            returnRangeValue: (value) {
                              setState(() {
                                returnRangeValue = value;
                                print("tims" + returnRangeValue.toString());
                                flightDuration = (returnRangeValue / 3600);
                                print("Flight Duration " +
                                    flightDuration.toString());
                                flightDurationInHours = flightDuration.floor();
                                print(
                                    "time" + flightDurationInHours.toString());
                              });
                            },
                            roundTripStatus: widget.roundTripStatus,
                          )
                        ])
                      : index == 3
                          ? Wrap(children: [
                              ResultPageBottomSheet(
                                index: index,
                                roundTripStatus: widget.roundTripStatus,
                                destinationFromName: widget.destinationFromName,
                                destinationToName: widget.destinationToName,
                                onTapSaveButton: () async {
                                  setState(() {
                                    showFloatingButton = true;
                                    selectedIndex[index] = true;
                                  });

                                  ObjectFactory().hiveBox.putDepartureFromTime(
                                      value: departureTimeFrom);
                                  ObjectFactory().hiveBox.putDepartureToTime(
                                      value: departureTimeTo);
                                  ObjectFactory()
                                      .hiveBox
                                      .putReturnFromTime(value: returnTimeFrom);
                                  ObjectFactory()
                                      .hiveBox
                                      .putReturnToTime(value: returnTimeTo);
                                  // await clearData();
                                  searchAPICallForFilter();
                                  Navigator.pop(context);

                                  //print("list Depart Flight Times"+[departureFlightTime1,departureFlightTime2,departureFlightTime3,departureFlightTime4].toString());
                                  //print("list Return Flight Times"+[returnFlightTime1,returnFlightTime2,returnFlightTime3,returnFlightTime4].toString());
                                },
                                departureToTime: (value) {
                                  print("to" + value.toString());
                                  setState(() {
                                    departureTimeTo =
                                        value.toStringAsFixed(0) + ":00";
                                  });
                                },
                                departureFromTime: (value) {
                                  print("from" + value.toString());

                                  setState(() {
                                    departureTimeFrom =
                                        value.toStringAsFixed(0) + ":00";
                                  });
                                },
                                returnToTime: (value) {
                                  setState(() {
                                    returnTimeTo =
                                        value.toStringAsFixed(0) + ":00";
                                  });
                                },
                                returnFromTime: (value) {
                                  setState(() {
                                    returnTimeFrom =
                                        value.toStringAsFixed(0) + ":00";
                                  });
                                },
                              )
                            ])
                          : index == 4
                              ? ResultPageBottomSheet(
                                  index: index,
                                  departureAirlinesArray: airlineDepartureArray,
                                  returnAirlinesArray: airlineReturnArray,
                                  onTapSaveButton: () async {
                                    setState(() {
                                      showFloatingButton = true;
                                      selectedIndex[index] = true;
                                      print("AirLines" +
                                          ObjectFactory()
                                              .hiveBox
                                              .getAirlineFilterSearch());
                                    });
                                    // await clearData();
                                    searchAPICallForFilter();
                                    Navigator.pop(context);
                                    ObjectFactory()
                                        .hiveBox
                                        .putSelectAllButtonStatus(value: null);
                                  },
                                )
                              : Wrap(children: [
                                  ResultPageBottomSheet(
                                    index: index,
                                    priceTotalArray: priceTotalArray,
                                    priceRangeLowerValue: (value) {
                                      setState(() {
                                        priceRangeLowerValue =
                                            int.parse(value.toStringAsFixed(0));
                                      });
                                    },
                                    priceRangeUpperValue: (value) {
                                      setState(() {
                                        priceRangeUpperValue =
                                            int.parse(value.toStringAsFixed(0));
                                      });
                                    },
                                    onTapSaveButton: () async {
                                      setState(() {
                                        showFloatingButton = true;
                                        selectedIndex[index] = true;
                                      });
                                      ObjectFactory()
                                          .hiveBox
                                          .putSelectedPriceRangeLowerForFilter(
                                              value: priceRangeLowerValue
                                                  .toDouble());
                                      ObjectFactory()
                                          .hiveBox
                                          .putSelectedPriceRangeUpperForFilter(
                                              value: priceRangeUpperValue
                                                  .toDouble());
                                      Navigator.pop(context);
                                      // await clearData();
                                      searchAPICallForFilter();
                                    },
                                  ),
                                ]);
        });
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
    );
  }

  void searchAPICallForFilter() {
    print(returnTimeTo.toString() + "stops");
    setState(() {
      switchingStatus = 0;
    });
    appBloc.searchFlights(
        searchReq: SearchRequest(
            flightType: widget.flightType,
            dateFrom: widget.dateFrom,
            dateTo: widget.dateTo,
            returnFrom: widget.returnFrom,
            returnTo: widget.returnTo,
            flyFrom: widget.flyFrom,
            flyTo: widget.flyTo,
            adults: widget.adults,
            children: widget.children,
            infants: widget.infants,
            selectedCabins: widget.selectedCabins,
            maxStopovers: stops != null ? stops : 5,
            locale: "en",
            maxFlyDuration: flightDurationInHours,
            dtimeFrom:
                departureTimeFrom == "24:00" ? "00:00" : departureTimeFrom,
            dtimeTo: departureTimeTo == "24:00" ? "00:00" : departureTimeTo,
            retDtimeFrom: returnTimeFrom == "24:00" ? "00:00" : returnTimeFrom,
            retDtimeTo: returnTimeTo == "24:00" ? "00:00" : returnTimeTo,
            curr: ObjectFactory().hiveBox.hiveGet(key: "currency_name") != null
                ? ObjectFactory()
                    .hiveBox
                    .hiveGet(key: "currency_name")
                    .split(" ")
                    .first
                : "USD",
            sort: ObjectFactory().hiveBox.getSortByForFilter() != null
                ? ObjectFactory().hiveBox.getSortByForFilter()
                : "price",
            selectAirlines:
                ObjectFactory().hiveBox.getAirlineFilterSearch() != null
                    ? ObjectFactory().hiveBox.getAirlineFilterSearch()
                    : "noValue",
            selectAirlinesExclude: false,
            priceFrom: priceRangeLowerValue,
            priceTo: priceRangeUpperValue));
  }
}
