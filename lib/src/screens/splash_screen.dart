import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/src/screens/home_page.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shimmer/shimmer.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  Timer _timerControl;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
          (route) => false);
    });
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "Splash_Screen", parameters: null);
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Constants.kitGradients[0],
        ),
        child: Scaffold(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            body: Builder(
                builder: (context) => SafeArea(
                    top: true,
                    left: true,
                    bottom: true,
                    child: Container(
                      height: screenHeight(context, dividedBy: 1),
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Constants.kitGradients[0].withOpacity(0.85),
                            Constants.kitGradients[0].withOpacity(0.9),
                            Constants.kitGradients[0],
                            Constants.kitGradients[0],
                          ],
                          tileMode: TileMode.repeated,
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Spacer(
                            flex: 1,
                          ),
                          Container(
                            height: screenWidth(context, dividedBy: 5),
                            width: screenWidth(context, dividedBy: 5),
                            child: SvgPicture.asset(
                              "assets/icons/splash_icon.svg",
                              fit: BoxFit.fill,
                            ),
                          ),
                          // Container(
                          //   height: screenWidth(context, dividedBy: 15),
                          //   width: screenWidth(context, dividedBy: 2),
                          //   child: SvgPicture.asset(
                          //     "assets/icons/all_flight_booking.svg",
                          //     fit: BoxFit.fill,
                          //   ),
                          // ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 80),
                          ),
                          Shimmer.fromColors(
                              child: Container(
                                height: screenWidth(context, dividedBy: 20),
                                width: screenWidth(context, dividedBy: 2),
                                child: SvgPicture.asset(
                                  "assets/icons/all_flight_booking.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                              baseColor: Constants.kitGradients[11],
                              highlightColor: Constants.kitGradients[17]),
                          Spacer(
                            flex: 1,
                          ),
                        ],
                      ),
                    )))));
  }
}
