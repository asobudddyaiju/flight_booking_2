import 'package:flightbooking/hive/hive.dart';
import 'package:flightbooking/src/resources/api_providers/repository/repository.dart';
import 'package:flightbooking/src/utils/urls.dart';
import 'api_client.dart';
import 'get_weekdays.dart';

/// it is a hub that connecting pref,repo,client
/// used to reduce imports in pages
class ObjectFactory {
  static final _objectFactory = ObjectFactory._internal();

  ObjectFactory._internal();

  factory ObjectFactory() => _objectFactory;

  ///Initialisation of Objects
  ApiClient _apiClient = ApiClient();
  Repository _repository = Repository();
  AppHive _hive = AppHive();
  GetDay _getDay = GetDay();
  Urls _getUrl = Urls();


  ///
  /// Getters of Objects
  ///
  ApiClient get apiClient => _apiClient;

  Repository get repository => _repository;

  AppHive get hiveBox => _hive;

  GetDay get getDay => _getDay;

  Urls get getUrl => _getUrl;


  ///
  /// Setters of Objects
  ///
  ///

}
