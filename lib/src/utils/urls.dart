class Urls {
  static final String baseUrl = "https://tequila-api.kiwi.com/";
  static final String destinationUrl = baseUrl+"locations/query?";
  static final String flightsSearchUrl = baseUrl+"v2/search?";
  static final String nearbySearchUrl = baseUrl+"locations/radius?";
  static final String bookingUrl = "https://www.kiwi.com/en/booking?";
}