import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class DetailsWidget2Body2Icon extends StatefulWidget {
  @override
  _DetailsWidget2Body2IconState createState() =>
      _DetailsWidget2Body2IconState();
}

class _DetailsWidget2Body2IconState extends State<DetailsWidget2Body2Icon> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: screenWidth(context, dividedBy: 19),
          height: screenHeight(context, dividedBy: 20),
          decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              shape: BoxShape.circle,
              border: Border.all(color: Constants.kitGradients[0], width: 3.0)),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 200),
        ),
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: Constants.kitGradients[24], shape: BoxShape.circle),
        ),
        Container(
          width: screenWidth(context, dividedBy: 19),
          height: screenHeight(context, dividedBy: 20),
          decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              shape: BoxShape.circle,
              border: Border.all(color: Constants.kitGradients[0], width: 3.0)),
        ),
      ],
    );
  }
}
