import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DetailsWidget4 extends StatefulWidget {
  final String notice;
  DetailsWidget4({this.notice});
  @override
  _DetailsWidget4State createState() => _DetailsWidget4State();
}

class _DetailsWidget4State extends State<DetailsWidget4> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 1.0),
     // height: screenHeight(context,dividedBy: 8.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: Constants.kitGradients[38]
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
           Container(
             width: screenWidth(context,dividedBy: 10),
            // height: screenHeight(context,dividedBy: 25),
            // color: Colors.greenAccent,
             child: Padding(
               padding: const EdgeInsets.all(2.0),
               child: SvgPicture.asset('assets/icons/details_caution.svg'),
             ),
           ),
            Container(
              width: screenWidth(context,dividedBy: 1.34),
             // height: screenHeight(context,dividedBy: 8),
             // color: Colors.yellow,
              child: RichText(
                text: TextSpan(
                    text: getTranslated(context, "Notice."),
                    style: TextStyle(
                        color: Constants.kitGradients[42], fontSize: 16, fontWeight: FontWeight.w700),
                    children: <TextSpan>[
                      TextSpan(text: widget.notice,
                          style: TextStyle(
                              color: Constants.kitGradients[42], fontSize: 16, fontWeight: FontWeight.w400),
                      )
                    ]
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
