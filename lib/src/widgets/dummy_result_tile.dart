import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/currency_symbol.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DummyResultTile extends StatefulWidget {
  final String oneWayDepartureTime;
  final String oneWayArrivalTime;
  final String oneWayDepartureAirportName;
  final String oneWayArrivalAirportName;
  final String oneWayTotalTime;
  final String oneWayStopsNumber;
  final String roundTripDepartureTime;
  final String roundTripArrivalTime;
  final String roundTripDepartureAirportName;
  final String roundTripAirportName;
  final String roundTripTotalTime;
  final String roundTripStopsNumber;
  final String price;
  final bool roundTripStatus;
  final String offersFound;
  final Function onTap;
  final List<String> resultPageAirlineArrayByIndex;

  DummyResultTile(
      {this.oneWayDepartureTime,
      this.onTap,
      this.oneWayDepartureAirportName,
      this.oneWayArrivalAirportName,
      this.oneWayArrivalTime,
      this.oneWayStopsNumber,
      this.oneWayTotalTime,
      this.roundTripAirportName,
      this.roundTripArrivalTime,
      this.roundTripDepartureAirportName,
      this.roundTripDepartureTime,
      this.roundTripStopsNumber,
      this.roundTripTotalTime,
      this.price,
      this.roundTripStatus,
      this.offersFound,
      this.resultPageAirlineArrayByIndex});

  @override
  _DummyResultTileState createState() => _DummyResultTileState();
}

class _DummyResultTileState extends State<DummyResultTile> {
  int resultPageAirlineArrayByIndexLength;
  @override
  void initState() {
    setState(() {
      resultPageAirlineArrayByIndexLength =
          widget.resultPageAirlineArrayByIndex.length;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: GestureDetector(
        onTap: () {
          widget.onTap();
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: Theme.of(context).cardColor,
          child: Container(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  vertical: screenHeight(context, dividedBy: 80)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          height: screenHeight(context, dividedBy: 30),
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: resultPageAirlineArrayByIndexLength,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: EdgeInsets.only(right: 5.0),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 16),
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(2.5),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                "https://images.kiwi.com/airlines/64/${widget.resultPageAirlineArrayByIndex[index]}.png"),
                                            fit: BoxFit.fill)),
                                  ),
                                );
                              })),
                      Text(
                        widget.offersFound + " offers from kiwi.com",
                        style: TextStyle(
                            color: Theme.of(context).primaryColorLight,
                            fontFamily: 'AirbnbCerealAppRegular',
                            fontSize: screenWidth(context, dividedBy: 40),
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  Divider(
                    thickness: 1,
                    color: Constants.kitGradients[2],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 7),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.oneWayDepartureTime,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 100),
                            ),
                            Text(
                              widget.oneWayDepartureAirportName,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context).primaryColorLight,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 18),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 6),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.oneWayStopsNumber,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.black,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w400),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical:
                                      screenHeight(context, dividedBy: 320)),
                              child: Icon(
                                Icons.arrow_forward,
                                size: 13,
                                color: Theme.of(context).primaryColorLight,
                              ),
                            ),
                            Text(
                              widget.oneWayTotalTime,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Theme.of(context).primaryColorLight,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 18),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 7),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.oneWayArrivalTime,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 100),
                            ),
                            Text(
                              widget.oneWayArrivalAirportName,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context).primaryColorLight,
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                      widget.roundTripStatus == false
                          ? Container(
                              width: screenWidth(context, dividedBy: 3.5),
                              child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      (ObjectFactory().hiveBox.hiveGet(
                                                      key: "currency_name") !=
                                                  null
                                              ? (getCurrencySymbol(
                                                  code: ObjectFactory()
                                                      .hiveBox
                                                      .hiveGet(
                                                          key: "currency_name")
                                                      .split(" ")
                                                      .first))
                                              : ("\$")) +
                                          widget.price,
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Constants.kitGradients[4],
                                          fontFamily: 'AirbnbCerealAppRegular',
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Container(
                              width: screenWidth(context, dividedBy: 7),
                            )
                    ],
                  ),
                  widget.roundTripStatus == true
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 40),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 7),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.roundTripDepartureTime,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w700),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 100),
                                      ),
                                      Text(
                                        widget.roundTripDepartureAirportName,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 18),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 6),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.roundTripStopsNumber,
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.black,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: screenHeight(context,
                                                dividedBy: 320)),
                                        child: Icon(
                                          Icons.arrow_forward,
                                          size: 13,
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                        ),
                                      ),
                                      Text(
                                        widget.roundTripTotalTime,
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 18),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 7),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.roundTripArrivalTime,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w700),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 100),
                                      ),
                                      Text(
                                        widget.roundTripAirportName,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                            fontFamily:
                                                'AirbnbCerealAppRegular',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 3.5),
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          (ObjectFactory().hiveBox.hiveGet(
                                                          key:
                                                              "currency_name") !=
                                                      null
                                                  ? (getCurrencySymbol(
                                                      code: ObjectFactory()
                                                          .hiveBox
                                                          .hiveGet(
                                                              key:
                                                                  "currency_name")
                                                          .split(" ")
                                                          .first))
                                                  : ("\$")) +
                                              widget.price,
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Constants.kitGradients[4],
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        )
                      : Container()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
