import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeBottomSheetWidget extends StatefulWidget {
  String title, subTitle;
  int count, adultCount, childrenCount, infantCount;
  ValueChanged countValue;
  HomeBottomSheetWidget(
      {this.title,
      this.subTitle,
      this.count,
      this.countValue,
      this.childrenCount,
      this.adultCount,
      this.infantCount});
  @override
  _HomeBottomSheetWidgetState createState() => _HomeBottomSheetWidgetState();
}

class _HomeBottomSheetWidgetState extends State<HomeBottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.08),
      height: screenHeight(context, dividedBy: 13),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 205),
              ),
              Text(
                widget.title,
                style: TextStyle(
                    fontFamily: "AirbnbCerealAppRegular",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Theme.of(context).hoverColor),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 205),
              ),
              Text(
                widget.subTitle,
                style: TextStyle(
                    fontFamily: 'AirbnbCerealAppRegular',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: Constants.kitGradients[1]),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 205),
              ),
            ],
          ),
          Row(
            children: [
              GestureDetector(
                child: Container(
                  width: screenWidth(context, dividedBy: 14),
                  height: screenHeight(context, dividedBy: 25),
                  child: Padding(
                    padding: const EdgeInsets.all(0.5),
                    child: SvgPicture.asset('assets/icons/minus.svg'),
                  ),
                ),
                onTap: () {
                  subtractCount(widget.count);
                  widget.countValue(widget.count);
                },
              ),
              Container(
                width: screenWidth(context, dividedBy: 10),
                height: screenHeight(context, dividedBy: 20),
                child: Center(
                  child: Text(
                    widget.count.toString(),
                    style: TextStyle(
                        fontFamily: 'Roboto',
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: Theme.of(context).textSelectionColor),
                  ),
                ),
              ),
              GestureDetector(
                child: Container(
                  width: screenWidth(context, dividedBy: 12.7),
                  height: screenHeight(context, dividedBy: 20),
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: SvgPicture.asset('assets/icons/plus.svg'),
                  ),
                ),
                onTap: () {
                  addCount(widget.count);
                  widget.countValue(widget.count);
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  void addCount(int sum) {
    if ((sum < 9) &&
        ((widget.adultCount + widget.childrenCount + widget.infantCount) < 9)) {
      sum = sum + 1;
      setState(() {
        widget.count = sum;
      });
    }
  }

  void subtractCount(int sum) {
    if (widget.title == "Adults" ? sum > 1 : sum > 0) {
      sum = sum - 1;
      setState(() {
        widget.count = sum;
      });
    }
  }
}
