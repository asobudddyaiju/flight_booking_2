import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreenBottomWidget2 extends StatefulWidget {
  String classes;
  bool selectionStatus;
  ValueChanged selectionStatusValue;
  HomeScreenBottomWidget2(
      {this.classes, this.selectionStatusValue, this.selectionStatus});
  @override
  _HomeScreenBottomWidget2State createState() =>
      _HomeScreenBottomWidget2State();
}

class _HomeScreenBottomWidget2State extends State<HomeScreenBottomWidget2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.08),
      height: screenHeight(context, dividedBy: 13),
      child: GestureDetector(
        onTap: () {
          setState(() {
            widget.selectionStatus = true;
          });
          widget.selectionStatusValue(widget.selectionStatus);
        },
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                widget.classes,
                style: TextStyle(
                    fontFamily: "AirbnbCerealAppRegular",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Theme.of(context).hoverColor),
              ),
              Container(
                  width: screenWidth(context, dividedBy: 15),
                  height: screenWidth(context, dividedBy: 15),
                  child: widget.selectionStatus == true
                      ? SvgPicture.asset("assets/icons/radio_button_yes.svg")
                      : SvgPicture.asset("assets/icons/radio_button_no.svg")),
            ]),
      ),
    );
  }
}
