import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/material/pickers/date_range_picker_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../utils/constants.dart';
import '../utils/util.dart';

class DateSelectionWidget extends StatefulWidget {
  String departDate,
      departYear,
      arrivalDate,
      arrivalYear,
      departureMonth,
      arrivalMonth,
      oneWayDepartDate,
      oneWayDepartMonth,
      oneWayDepartYear;
  bool roundTripStatus;
  DateSelectionWidget(
      {this.arrivalYear,
      this.arrivalDate,
      this.departDate,
      this.departYear,
      this.roundTripStatus,
      this.arrivalMonth,
      this.departureMonth,
      this.oneWayDepartDate,
      this.oneWayDepartMonth,
      this.oneWayDepartYear});

  @override
  _DateSelectionWidgetState createState() => _DateSelectionWidgetState();
}

DateTime firstDate;

DateTime lastDate;

DateTime currentDate;

DatePickerEntryMode initialEntryMode;

String helpText;

String cancelText;

String confirmText;

String saveText;

String errorFormatText;

String errorInvalidText;

String errorInvalidRangeText;

String fieldStartLabelText;

String fieldEndLabelText;

double width;

EdgeInsets margin;

DateTimeRange initialValue;

// DateFormat dateFormat;

class _DateSelectionWidgetState extends State<DateSelectionWidget> {
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        initialDateRange: initialValue,
        firstDate: firstDate ?? DateTime.now(),
        lastDate: lastDate ?? DateTime(DateTime.now().year + 5),
        helpText: helpText ?? getTranslated(context, "Select_Date_Range"),
        cancelText: cancelText ?? getTranslated(context, "CANCEL"),
        confirmText: confirmText ?? getTranslated(context, "OK"),
        saveText: saveText ?? getTranslated(context, "SAVE"),
        errorFormatText:
            errorFormatText ?? getTranslated(context, "Invalid_format"),
        errorInvalidText:
            errorInvalidText ?? getTranslated(context, "Out_of_range"),
        errorInvalidRangeText:
            errorInvalidRangeText ?? getTranslated(context, "Invalid_range"),
        fieldStartHintText:
            fieldStartLabelText ?? getTranslated(context, "Start_Date"),
        fieldEndLabelText:
            fieldEndLabelText ?? getTranslated(context, "End_Date"));
    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  void initState() {
    initialValue = DateTimeRange(
      start: DateTime.now().add(Duration(days: 0)),
      end: DateTime.now().add(Duration(days: 1)),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        color: Colors.transparent,
        width: screenWidth(context, dividedBy: 1.2),
        height: screenHeight(context, dividedBy: 10.0),
        child: Center(
          child: Row(
            children: [
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 27),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenHeight(context, dividedBy: 23),
                    child: SvgPicture.asset('assets/icons/calendar_icon.svg'),
                  ),
                ],
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 60),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    getTranslated(context, "Departure"),
                    style: TextStyle(
                        fontFamily: "AirbnbCerealAppRegular",
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: Constants.kitGradients[1]),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  RichText(
                      text: TextSpan(
                          text: widget.roundTripStatus == true
                              ? (widget.departDate +
                                  " " +
                                  getTranslated(context, widget.departureMonth))
                              : (widget.oneWayDepartDate +
                                  " " +
                                  getTranslated(
                                      context, widget.oneWayDepartMonth)),
                          style: TextStyle(
                              fontFamily: 'AirbnbCerealAppRegular',
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w700,
                              fontSize: 16,
                              color: Theme.of(context).textSelectionColor),
                          children: <TextSpan>[
                        TextSpan(
                          text: " " +
                              (widget.roundTripStatus == true
                                  ? widget.departYear
                                  : widget.oneWayDepartYear),
                          style: TextStyle(
                              fontFamily: 'AirbnbCerealAppRegular',
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Theme.of(context).textSelectionColor),
                        )
                      ])),
                ],
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 10),
              ),
              widget.roundTripStatus == true
                  ? Container(
                      width: screenWidth(context, dividedBy: 40),
                      height: screenHeight(context, dividedBy: 23),
                    )
                  : Container(),
              SizedBox(
                width: screenWidth(context, dividedBy: 25),
              ),
              widget.roundTripStatus == true
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          getTranslated(context, "Return"),
                          style: TextStyle(
                              fontFamily: 'AirbnbCerealAppRegular',
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Constants.kitGradients[1]),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 80),
                        ),
                        RichText(
                            text: TextSpan(
                                text: widget.arrivalDate +
                                    " " +
                                    getTranslated(context, widget.arrivalMonth),
                                style: TextStyle(
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    color:
                                        Theme.of(context).textSelectionColor),
                                children: <TextSpan>[
                              TextSpan(
                                text: " " + widget.arrivalYear,
                                style: TextStyle(
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16,
                                    color:
                                        Theme.of(context).textSelectionColor),
                              )
                            ])),
                      ],
                    )
                  : Container(),
            ],
          ),
        ),
      ),
      onTap: () {
        selectDateRange(context);
      },
    );
  }
}
