import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/material/pickers/date_range_picker_dialog.dart';


import '../utils/util.dart';

class DateSelectionBox extends StatefulWidget {

  @override
  _DateSelectionBoxState createState() => _DateSelectionBoxState();
}
 DateTime firstDate;

 DateTime lastDate;

 DateTime currentDate;

 DatePickerEntryMode initialEntryMode;

 String helpText;

 String cancelText;

 String confirmText;

 String saveText;

 String errorFormatText;

 String errorInvalidText;

 String errorInvalidRangeText;


 String fieldStartLabelText;

 String fieldEndLabelText;

 double width;

 EdgeInsets margin;

 DateTimeRange initialValue;

 // DateFormat dateFormat;

String fieldVal;
class _DateSelectionBoxState extends State<DateSelectionBox> {
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        initialDateRange: initialValue,
        firstDate: firstDate ?? DateTime.now(),
        lastDate: lastDate ?? DateTime(DateTime.now().year + 5),
        helpText: helpText ?? 'Select Date Range',
        cancelText: cancelText ?? 'CANCEL',
        confirmText: confirmText ?? 'OK',
        saveText: saveText ?? 'SAVE',
        errorFormatText: errorFormatText ?? 'Invalid format.',
        errorInvalidText: errorInvalidText ?? 'Out of range.',
        errorInvalidRangeText:
        errorInvalidRangeText ?? 'Invalid range.',
        fieldStartHintText: fieldStartLabelText ?? 'Start Date',
        fieldEndLabelText: fieldEndLabelText ?? 'End Date');
    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }
  @override
  void initState() {
initialValue= DateTimeRange(
  start: DateTime.now().add(Duration(days:0)),
  end: DateTime.now().add(Duration(days:1)),
);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
           height: screenHeight(context,dividedBy: 7),
           width: screenWidth(context,dividedBy: 1.03),
           color:Constants.kitGradients[1],
           child:
           // DateRangeField(
           //   context: context,
           //   initialValue:
           //   DateTimeRange(
           //     start: DateTime.now().add(Duration(days:0)),
           //     end: DateTime.now().add(Duration(days:1)),
           //   ),
           // )
           Row(
             mainAxisAlignment: MainAxisAlignment.start,
             children: [

               Padding(
                 padding:  EdgeInsets.only(),
                 child:
                 // DateRangeField(
                 //   context: context,
                 //   initialValue: DateTimeRange(
                 //     start: DateTime.now().add(Duration(days:0)),
                 //     end: DateTime.now().add(Duration(days:1)),
                 //   ),
                 // )
                  GestureDetector(
                      onTap: (){
                        selectDateRange(context);
                      },
                      child: Text(Hive.box('box').get(3)))
                 ,

               ),

               SizedBox(
                 width: screenWidth(context,dividedBy: 9),


               ),

               Padding(
                 padding: EdgeInsets.only(left: screenWidth(context,dividedBy:20),top:screenHeight(context,dividedBy: 50),bottom: screenWidth(context,dividedBy: 20),
                 ),
                 child: VerticalDivider(
                   color:Constants.kitGradients[13],
                   thickness: 2,
                 ),
               ),

               Padding(
                 padding:  EdgeInsets.only(),
                 child:Text(Hive.box('box').get(4) )
               ),
               SizedBox(
                 width: screenWidth(context,dividedBy: 10),
                 child: Container(
                 ),
               ),
               ],
           ),
           ),
    );
  }
}


