import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreenDateSelectionBox extends StatefulWidget {
  final String heading;
  final String date;
  final String year;
  final bool icon;

  HomeScreenDateSelectionBox({this.date, this.heading, this.year, this.icon});

  @override
  _HomeScreenDateSelectionBoxState createState() =>
      _HomeScreenDateSelectionBoxState();
}

class _HomeScreenDateSelectionBoxState
    extends State<HomeScreenDateSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 13),
      width: screenWidth(context, dividedBy: 2.31),
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 60)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            widget.icon == false
                ? Container(
                    width: screenWidth(context, dividedBy: 20),
                  )
                : SvgPicture.asset(
                    "assets/icons/calendar_icon.svg",
                  ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 40)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.heading,
                    style: TextStyle(
                        color: Constants.kitGradients[1],
                        fontFamily: 'AirbnbCerealRegular',
                        fontSize: 12,
                        fontWeight: FontWeight.w400),
                  ),
                  RichText(
                    text: TextSpan(
                      text: widget.date + " ",
                      style: TextStyle(
                          color: Theme.of(context).textSelectionColor,
                          fontFamily: 'AirbnbCerealRegular',
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                      children: <TextSpan>[
                        TextSpan(
                          text: widget.year,
                          style: TextStyle(
                              color: Theme.of(context).textSelectionColor,
                              fontFamily: 'AirbnbCerealRegular',
                              fontWeight: FontWeight.w300,
                              fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
