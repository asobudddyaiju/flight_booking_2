import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/src/screens/destination_selection_from.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DestinationSelectionBox extends StatefulWidget {
  String icon;
  String heading;
  String destination;
  String destinationShort;
  bool destinationTo;

  DestinationSelectionBox(
      {this.heading, this.icon, this.destination, this.destinationShort, this.destinationTo});

  @override
  _DestinationSelectionBoxState createState() =>
      _DestinationSelectionBoxState();
}

class _DestinationSelectionBoxState extends State<DestinationSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        FirebaseAnalytics().logEvent(name:"clicked_destination_from_selection_widget",parameters: null);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DestinationFrom(destinationTo: widget.destinationTo,)));
      },
      child: Container(
        height: screenHeight(context, dividedBy: 10),
        child: Row(
          children: [
            Column(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 10),
                  height: screenHeight(context, dividedBy: 10),
                  decoration: BoxDecoration(
                    color: Constants.kitGradients[0],
                    shape: BoxShape.circle,
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: SvgPicture.asset(
                      widget.icon,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Container(
              height: screenHeight(context, dividedBy: 10),
              width: screenWidth(context, dividedBy: 1.26),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.heading,
                    style: TextStyle(
                        color: Colors.white70,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                  RichText(
                    text: TextSpan(
                      text: widget.destinationShort + " - ",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'AirbnbCerealAppRegular',
                          fontWeight: FontWeight.w700,
                          fontSize: 18),
                      children: <TextSpan>[
                        TextSpan(
                          text: widget.destination,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'AirbnbCerealRegular',
                              fontWeight: FontWeight.w400,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    thickness: screenHeight(context, dividedBy: 400),
                    color: Constants.kitGradients[0],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
