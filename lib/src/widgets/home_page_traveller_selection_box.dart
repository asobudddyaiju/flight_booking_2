import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TravellerSelectionBox extends StatefulWidget {
  final String icon;
  final int adultNo;
  final int childrenNo;
  final int infant;
  final String heading;
  final bool traveller;
  final String subHeading;
  final bool arrowUpward;
  Function onTap;
  Function bottomSheet;

  TravellerSelectionBox(
      {this.icon,
      this.adultNo,
      this.childrenNo,
      this.infant,
      this.heading,
      this.traveller,
      this.subHeading,
      this.onTap,this.arrowUpward,this.bottomSheet});
  @override
  _TravellerSelectionBoxState createState() => _TravellerSelectionBoxState();
}

class _TravellerSelectionBoxState extends State<TravellerSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: widget.onTap,
          child: Container(
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 13),
            color: Colors.transparent,
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      SvgPicture.asset(widget.icon),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: widget.traveller == true
                            ? screenWidth(context, dividedBy: 40)
                            : screenWidth(context, dividedBy: 35)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1.57),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.heading,
                            style: TextStyle(
                                color: Constants.kitGradients[1],
                                fontFamily: 'AirbnbCerealRegular',
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                          ),
                          widget.traveller == true
                              ? RichText(
                                  text: TextSpan(
                                    text: widget.adultNo.toString() +
                                        (widget.adultNo <= 1?" Adult, ":" Adults, ") +
                                        widget.childrenNo.toString() +
                                        (widget.childrenNo <= 1?" child, ":" children, ") +
                                        widget.infant.toString() +
                                        (widget.infant <= 1?" infant":" infants"),
                                    style: TextStyle(
                                        color: Theme.of(context).textSelectionColor,
                                        fontFamily: 'AirbnbCerealRegular',
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                    children: <TextSpan>[],
                                  ),
                                )
                              : RichText(
                                  text: TextSpan(
                                    text: widget.subHeading,
                                    style: TextStyle(
                                        color: Theme.of(context).textSelectionColor,
                                        fontFamily: 'AirbnbCerealRegular',
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                    children: <TextSpan>[],
                                  ),
                                )
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [

                      SizedBox(height: screenHeight(context,dividedBy:30 ),),
                      SvgPicture.asset(
                        "assets/icons/arrow_down.svg",width: screenWidth(context,dividedBy: 25,),
                        color: Constants.kitGradients[0],
                      )
                    ],
                  ),

                ],
              ),
            ),
          ),
        ),

        Divider(thickness: 1,color: Theme.of(context).dividerColor,),
      ],
    );
  }
}
