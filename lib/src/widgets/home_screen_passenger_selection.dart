import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreenPassengerSelection extends StatefulWidget {
  String travelClass, numberOfPassengers;
  Function onTap;
  HomeScreenPassengerSelection({this.numberOfPassengers, this.travelClass, this.onTap});
  @override
  _HomeScreenPassengerSelectionState createState() =>
      _HomeScreenPassengerSelectionState();
}

class _HomeScreenPassengerSelectionState
    extends State<HomeScreenPassengerSelection> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: screenWidth(context, dividedBy: 1.2),
        height: screenHeight(context, dividedBy: 10.0),
        decoration: BoxDecoration(
            color: Constants.kitGradients[15],
            borderRadius: BorderRadius.circular(15)),
        child: Center(
          child: Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 25),
              ),
              SvgPicture.asset('assets/icons/home_screen_user.svg'),
              SizedBox(
                width: screenWidth(context, dividedBy: 25),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                   getTranslated(context, widget.travelClass),
                    style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: Constants.kitGradients[18]),
                  ),
                  Text(
                    widget.numberOfPassengers,
                    style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        color: Constants.kitGradients[28]),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
