import 'package:flightbooking/src/screens/home_page.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ResultAppBarWidget extends StatefulWidget {
  final String dateTimePassengers, routeFrom, routeTo;

  ResultAppBarWidget({this.dateTimePassengers, this.routeFrom, this.routeTo});

  @override
  _ResultAppBarWidgetState createState() => _ResultAppBarWidgetState();
}

class _ResultAppBarWidgetState extends State<ResultAppBarWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Constants.kitGradients[0].withOpacity(0.85),
            Constants.kitGradients[0].withOpacity(0.9),
            Constants.kitGradients[0],
            Constants.kitGradients[0],
          ],
          tileMode: TileMode.repeated,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                GestureDetector(
                  child: Container(
                      height: screenHeight(context, dividedBy: 25),
                      color: Colors.transparent,
                      child: SvgPicture.asset(
                        'assets/icons/back.svg',
                        color: Constants.kitGradients[11],
                      )),
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
                  },
                ),
                Spacer(
                  flex: 1,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 45),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1.4),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                widget.routeFrom + " - " + widget.routeTo,
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  color: Constants.kitGradients[11],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Text(
                      widget.dateTimePassengers,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'AirbnbCerealAppRegular',
                        color: Constants.kitGradients[14],
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
