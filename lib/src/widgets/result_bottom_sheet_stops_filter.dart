import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetStopsFilter extends StatefulWidget {
  // List<int> durationDepartureArray = [];
  // List<int> durationReturnArray = [];
  ValueChanged showSaveButtonValueChanged;
  ValueChanged stops;
  // ValueChanged returnRangeValue;
  // bool roundTripStatus;

  ResultPageBottomSheetStopsFilter({
    // {this.durationDepartureArray,
    //   this.durationReturnArray,
    this.showSaveButtonValueChanged,
    this.stops,
    // this.returnRangeValue,
    // this.roundTripStatus
  });

  @override
  _ResultPageBottomSheetStopsFilterState createState() =>
      _ResultPageBottomSheetStopsFilterState();
}

class _ResultPageBottomSheetStopsFilterState
    extends State<ResultPageBottomSheetStopsFilter> {
  // double _lowerValue;
  double stops = 5;
  double dummyStop = 0;

  // double _upperValue;

  // double departRangeStartsAt;
  // double returnRangeEndsAt;
  // bool checkDepartureRangeIsLowerThanReturn;
  // double variableForObjectFactoryDeparture;
  // double variableForObjectFactoryReturn;
  // double x;
  // double y;

  @override
  void initState() {
    if (ObjectFactory().hiveBox.getStopsNumber() != null)
      stops = int.parse(ObjectFactory().hiveBox.getStopsNumber()).toDouble();

    // if (widget.roundTripStatus == true) {
    // setState(() {
    // checkDepartureRangeIsLowerThanReturn = (ObjectFactory()
    //     .hiveBox
    //     .getDepartureArrayMax()
    //     .roundToDouble()) <=
    //     (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //     ? true
    //     : false;

    // departRangeStartsAt = (ObjectFactory()
    //     .hiveBox
    //     .getDepartureArrayMin()
    //     .roundToDouble()) <=
    //     (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //     ? (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble())
    //     : (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble());

    //   returnRangeEndsAt = (ObjectFactory()
    //       .hiveBox
    //       .getReturnArrayMax()
    //       .roundToDouble()) >=
    //       (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble())
    //       ? (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //       : (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble());
    //
    //   x = (departRangeStartsAt) / 100000;
    //   y = (returnRangeEndsAt) / 100000;
    // });

    // if (ObjectFactory().hiveBox.getDepartureRangeStart() != null) {
    //   if (ObjectFactory().hiveBox.getDepartureRangeLowerThanReturnRange() ==
    //       true) {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _lowerValue = variableForObjectFactoryDeparture;
    //       _upperValue = variableForObjectFactoryReturn;
    //     });
    //   } else {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _upperValue = variableForObjectFactoryDeparture;
    //       _lowerValue = variableForObjectFactoryReturn;
    //     });
    //   }

    // print("data is from object factory "+_lowerValue.toString());
    // print("data is from object factory "+_upperValue.toString());
    //   } else {
    //     setState(() {
    //       _lowerValue = (departRangeStartsAt);
    //       _upperValue = (returnRangeEndsAt);
    //     });
    //
    //     // print("data is not from object factory "+_lowerValue.toString());
    //     // print("data is not from object factory "+_upperValue.toString());
    //   }
    // } else {
    //   setState(() {
    //     checkDepartureRangeIsLowerThanReturn = true;
    //     departRangeStartsAt =
    //         ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble();
    //     returnRangeEndsAt =
    //         ObjectFactory().hiveBox.getDepartureArrayMax().roundToDouble();
    //     x = (departRangeStartsAt) / 100000;
    //     y = (returnRangeEndsAt) / 100000;
    //   });
    //   if (ObjectFactory().hiveBox.getDepartureRangeStart() != null) {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _lowerValue = variableForObjectFactoryDeparture;
    //       _upperValue = variableForObjectFactoryReturn;
    //     });
    //     // print("data is from object factory "+_lowerValue.toString());
    //     // print("data is from object factory "+_upperValue.toString());
    //   } else {
    //     setState(() {
    //       _lowerValue = (departRangeStartsAt + x);
    //       _upperValue = (returnRangeEndsAt - y);
    //     });

    // print("data is not from object factory "+_lowerValue.toString());
    // print("data is not from object factory "+_upperValue.toString());
    //   }
    // }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "0",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context).hoverColor),
                ),
                Text(
                  stops.toStringAsFixed(0),
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context).hoverColor),
                ),
              ],
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: Constants.kitGradients[0],
                activeTickMarkColor: Constants.kitGradients[0],
                activeTrackColor: Constants.kitGradients[0],
                inactiveTrackColor: Colors.grey,
                //trackHeight: 8.0,
                thumbColor: Constants.kitGradients[0],
                valueIndicatorColor: Constants.kitGradients[0],
                showValueIndicator: false
                    ? ShowValueIndicator.always
                    : ShowValueIndicator.onlyForDiscrete,
              ),
              child: Slider(
                min: 0,
                max: 5,
                value: stops,
                onChanged: (value) {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    stops = value;
                  });
                  widget.stops(value);
                },
              ),
            )
            // SliderTheme(
            //   data: SliderTheme.of(context).copyWith(
            //     overlayColor: Constants.kitGradients[0],
            //     activeTickMarkColor: Constants.kitGradients[0],
            //     activeTrackColor: Constants.kitGradients[0],
            //     inactiveTrackColor: Colors.grey,
            //     //trackHeight: 8.0,
            //     thumbColor: Constants.kitGradients[0],
            //     valueIndicatorColor: Constants.kitGradients[0],
            //     showValueIndicator: false
            //         ? ShowValueIndicator.always
            //         : ShowValueIndicator.onlyForDiscrete,
            //   ),
            //   child: frs.RangeSlider(
            //     min: 0,
            //     max: 5,
            //     lowerValue: dummyStop.toDouble(),
            //     upperValue: stops.toDouble(),
            //     showValueIndicator: true,
            //     onChanged: (double newLowerValue, double newUpperValue) {
            //       setState(() {
            //         stops = newUpperValue;
            //       });
            //     },
            //     onChangeStart: (double startLowerValue, double startUpperValue) {
            //       widget.showSaveButtonValueChanged(true);
            //     },
            //     onChangeEnd: (double newLowerValue, double newUpperValue) {
            //       widget.stops(newUpperValue);
            //       // widget.showSaveButtonValueChanged(true);
            //       // if (widget.roundTripStatus == true) {
            //       //   if (checkDepartureRangeIsLowerThanReturn == true) {
            //       //     widget.departRangeValue(newLowerValue);
            //       //     widget.returnRangeValue(newUpperValue);
            //       //     ObjectFactory()
            //       //         .hiveBox
            //       //         .putDepartureRangeLowerThanReturnRange(value: true);
            //       //   } else {
            //       //     widget.departRangeValue(newUpperValue);
            //       //     widget.returnRangeValue(newLowerValue);
            //       //     ObjectFactory()
            //       //         .hiveBox
            //       //         .putDepartureRangeLowerThanReturnRange(value: false);
            //       //   }
            //       // } else {
            //       //   widget.departRangeValue(newLowerValue);
            //       //   widget.returnRangeValue(newUpperValue);
            //       //   ObjectFactory()
            //       //       .hiveBox
            //       //       .putDepartureRangeLowerThanReturnRange(value: true);
            //       // }
            //     },
            //   ),
            // ),
            ),
        SizedBox(
          height: screenHeight(context, dividedBy: 10.0),
        ),
      ],
    );
  }
}
