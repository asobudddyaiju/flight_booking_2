import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_bottom_sheet_flight_times.dart';
import 'package:flightbooking/src/widgets/result_bottom_sheet_stops_filter.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_for_airlines.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_for_duration.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_for_price.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_widget_for_cheapest.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheet extends StatefulWidget {
  final int index;
  Function onTapCheapest, onTapQuickest;
  Function onTapSaveButton;
  bool tickCheapest, tickQuickest;
  List<int> durationDepartureArray = [];
  List<int> durationReturnArray = [];
  ValueChanged departRangeValue;
  ValueChanged returnRangeValue;

  ValueChanged departureFlightTime1;
  ValueChanged departureFlightTime2;
  ValueChanged departureFlightTime3;
  ValueChanged departureFlightTime4;
  ValueChanged departureFromTime;
  ValueChanged departureToTime;

  ValueChanged returnFlightTime1;
  ValueChanged returnFlightTime2;
  ValueChanged returnFlightTime3;
  ValueChanged returnFlightTime4;
  ValueChanged returnFromTime;
  ValueChanged returnToTime;

  ValueChanged priceRangeLowerValue;
  ValueChanged priceRangeUpperValue;

  ValueChanged direct;
  ValueChanged stops;
  ValueChanged tickOneStop;
  ValueChanged tickTwoPlusStops;

  bool roundTripStatus;
  String destinationFromName;
  String destinationToName;

  List<String> departureAirlinesArray;
  List<String> returnAirlinesArray;

  List<int> priceTotalArray;

  ResultPageBottomSheet(
      {this.index,
      this.onTapCheapest,
      this.onTapQuickest,
      this.tickQuickest,
      this.tickCheapest,
      this.onTapSaveButton,
      this.durationReturnArray,
      this.durationDepartureArray,
      this.departRangeValue,
      this.returnRangeValue,
      this.roundTripStatus,
      this.destinationToName,
      this.destinationFromName,
      this.departureFlightTime1,
      this.departureFlightTime2,
      this.departureFlightTime3,
      this.departureFlightTime4,
      this.departureToTime,
      this.departureFromTime,
      this.returnToTime,
      this.returnFromTime,
      this.returnFlightTime1,
      this.returnFlightTime2,
      this.returnFlightTime3,
      this.returnFlightTime4,
      this.returnAirlinesArray,
      this.departureAirlinesArray,
      this.priceTotalArray,
      this.priceRangeUpperValue,
      this.priceRangeLowerValue,
      this.direct,
      this.tickOneStop,
      this.stops,
      this.tickTwoPlusStops});

  @override
  _ResultPageBottomSheetState createState() => _ResultPageBottomSheetState();
}

class _ResultPageBottomSheetState extends State<ResultPageBottomSheet> {
  bool showSaveButton;

  @override
  void initState() {
    setState(() {
      showSaveButton = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: widget.index == 4 ? screenHeight(context, dividedBy: 1.6) : null,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
          color: Theme.of(context).scaffoldBackgroundColor),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: screenHeight(context, dividedBy: 15),
                      decoration: BoxDecoration(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          borderRadius: BorderRadius.circular(6.0)),
                      child: Center(
                          child: Text(
                        'Cancel',
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'AirbnbCerealAppRegular',
                            fontWeight: FontWeight.w700,
                            color: Constants.kitGradients[0]),
                      )),
                    ),
                  ),
                  showSaveButton == true
                      ? GestureDetector(
                          onTap: widget.onTapSaveButton,
                          child: Container(
                            width: screenWidth(context, dividedBy: 4.0),
                            height: screenHeight(context, dividedBy: 15),
                            decoration: BoxDecoration(
                                color: Constants.kitGradients[0],
                                borderRadius: BorderRadius.circular(6.0)),
                            child: Center(
                                child: Text(
                              'Save',
                              style: TextStyle(
                                  fontSize: 17.0,
                                  fontFamily: 'ProximaNova',
                                  fontWeight: FontWeight.w700,
                                  color: Constants.kitGradients[11]),
                            )),
                          ),
                        )
                      : Container(
                          width: screenWidth(context, dividedBy: 4.0),
                          height: screenHeight(context, dividedBy: 15),
                        ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
            widget.index == 0
                ? ResultPageBottomSheetForCheapest(
                    onTapCheapest: widget.onTapCheapest,
                    onTapQuickest: widget.onTapQuickest,
                    tickCheapest: widget.tickCheapest,
                    tickQuickest: widget.tickQuickest,
                  )
                : widget.index == 1
                    ?
                    // ResultPageBottomSheetWidgetForStops(
                    //             showSaveButtonValueChanged: (value) {
                    //               setState(() {
                    //                 showSaveButton = value;
                    //               });
                    //             },
                    //             direct: widget.direct,
                    //             tickOneStop: widget.tickOneStop,
                    //             tickTwoPlusStops: widget.tickTwoPlusStops,
                    //           )
                    ResultPageBottomSheetStopsFilter(
                        showSaveButtonValueChanged: (value) {
                          setState(() {
                            showSaveButton = value;
                          });
                        },
                        stops: (value) {
                          widget.stops(value);
                        },
                      )
                    : widget.index == 2
                        ? ResultPageBottomSheetForDuration(
                            durationDepartureArray:
                                widget.durationDepartureArray,
                            durationReturnArray: widget.durationReturnArray,
                            showSaveButtonValueChanged: (value) {
                              setState(() {
                                showSaveButton = value;
                              });
                            },
                            departRangeValue: (value) {
                              widget.departRangeValue(value);
                            },
                            returnRangeValue: (value) {
                              widget.returnRangeValue(value);
                            },
                            roundTripStatus: widget.roundTripStatus,
                          )
                        : widget.index == 3
                            ?
                            // ResultPageBottomSheetFlightTimes(
                            //                     showSaveButtonValueChanged: (value) {
                            //                       setState(() {
                            //                         showSaveButton = value;
                            //                       });
                            //                     },
                            //                     roundTripStatus: widget.roundTripStatus,
                            //                     destinationToName: widget.destinationToName,
                            //                     destinationFromName: widget.destinationFromName,
                            //                     departureFlightTime1:
                            //                         widget.departureFlightTime1,
                            //                     departureFlightTime2:
                            //                         widget.departureFlightTime2,
                            //                     departureFlightTime3:
                            //                         widget.departureFlightTime3,
                            //                     departureFlightTime4:
                            //                         widget.departureFlightTime4,
                            //                     returnFlightTime1: widget.returnFlightTime1,
                            //                     returnFlightTime2: widget.returnFlightTime2,
                            //                     returnFlightTime3: widget.returnFlightTime3,
                            //                     returnFlightTime4: widget.returnFlightTime4,
                            //                   )
                            ResultPageBottomSheetFlightTime(
                                roundTripStatus: widget.roundTripStatus,
                                destinationFromName: widget.destinationFromName,
                                destinationToName: widget.destinationToName,
                                returnFromTime: (value) {
                                  widget.returnFromTime(value);
                                },
                                returnToTime: (value) {
                                  widget.returnToTime(value);
                                },
                                showSaveButtonValueChanged: (value) {
                                  setState(() {
                                    showSaveButton = value;
                                  });
                                },
                                departureFromTime: (value) {
                                  widget.departureFromTime(value);
                                },
                                departureToTime: (value) {
                                  widget.departureToTime(value);
                                },
                              )
                            : widget.index == 4
                                ? Expanded(
                                    child: ResultPageBottomSheetForAirlines(
                                      returnAirlinesArray:
                                          widget.returnAirlinesArray,
                                      departureAirlinesArray:
                                          widget.departureAirlinesArray,
                                      showSaveButtonValueChanged: (value) {
                                        setState(() {
                                          showSaveButton = value;
                                        });
                                      },
                                    ),
                                  )
                                : widget.index == 5
                                    ? ResultPageBottomSheetForPrice(
                                        priceTotalArray: widget.priceTotalArray,
                                        showSaveButtonValueChanged: (value) {
                                          setState(() {
                                            showSaveButton = value;
                                          });
                                        },
                                        priceRangeLowerValue: (value) {
                                          widget.priceRangeLowerValue(value);
                                        },
                                        priceRangeUpperValue: (value) {
                                          widget.priceRangeUpperValue(value);
                                        })
                                    : Container()
          ],
        ),
      ),
    );
  }
}
