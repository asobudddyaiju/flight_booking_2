import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_flight_times_time_box_widget.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetFlightTimes extends StatefulWidget {
  ValueChanged showSaveButtonValueChanged;
  bool roundTripStatus;
  String destinationFromName;
  String destinationToName;
  ValueChanged departureFlightTime1;
  ValueChanged departureFlightTime2;
  ValueChanged departureFlightTime3;
  ValueChanged departureFlightTime4;

  ValueChanged returnFlightTime1;
  ValueChanged returnFlightTime2;
  ValueChanged returnFlightTime3;
  ValueChanged returnFlightTime4;

  ResultPageBottomSheetFlightTimes(
      {this.showSaveButtonValueChanged,
      this.roundTripStatus,
      this.destinationFromName,
      this.destinationToName,
      this.departureFlightTime1,
      this.departureFlightTime2,
      this.departureFlightTime3,
      this.departureFlightTime4,
      this.returnFlightTime1,
      this.returnFlightTime2,
      this.returnFlightTime3,
      this.returnFlightTime4});

  @override
  _ResultPageBottomSheetFlightTimesState createState() =>
      _ResultPageBottomSheetFlightTimesState();
}

class _ResultPageBottomSheetFlightTimesState
    extends State<ResultPageBottomSheetFlightTimes> {
  bool departureFlightTime1,
      departureFlightTime2,
      departureFlightTime3,
      departureFlightTime4,
      returnFlightTime1,
      returnFlightTime2,
      returnFlightTime3,
      returnFlightTime4;
  List<bool> departureFlightTimeStatusArray = [];

  @override
  void initState() {
    ObjectFactory().hiveBox.getDepartureFlightTime1() != null
        ? setState(() {
            departureFlightTime1 =
                ObjectFactory().hiveBox.getDepartureFlightTime1();
          })
        : setState(() {
            departureFlightTime1 = false;
          });

    ObjectFactory().hiveBox.getDepartureFlightTime2() != null
        ? setState(() {
            departureFlightTime2 =
                ObjectFactory().hiveBox.getDepartureFlightTime2();
          })
        : setState(() {
            departureFlightTime2 = false;
          });

    ObjectFactory().hiveBox.getDepartureFlightTime3() != null
        ? setState(() {
            departureFlightTime3 =
                ObjectFactory().hiveBox.getDepartureFlightTime3();
          })
        : setState(() {
            departureFlightTime3 = false;
          });

    ObjectFactory().hiveBox.getDepartureFlightTime4() != null
        ? setState(() {
            departureFlightTime4 =
                ObjectFactory().hiveBox.getDepartureFlightTime4();
          })
        : setState(() {
            departureFlightTime4 = false;
          });

    ObjectFactory().hiveBox.getReturnFlightTime1() != null
        ? setState(() {
            returnFlightTime1 = ObjectFactory().hiveBox.getReturnFlightTime1();
          })
        : setState(() {
            returnFlightTime1 = false;
          });

    ObjectFactory().hiveBox.getReturnFlightTime2() != null
        ? setState(() {
            returnFlightTime2 = ObjectFactory().hiveBox.getReturnFlightTime2();
          })
        : setState(() {
            returnFlightTime2 = false;
          });

    ObjectFactory().hiveBox.getReturnFlightTime3() != null
        ? setState(() {
            returnFlightTime3 = ObjectFactory().hiveBox.getReturnFlightTime3();
          })
        : setState(() {
            returnFlightTime3 = false;
          });

    ObjectFactory().hiveBox.getReturnFlightTime4() != null
        ? setState(() {
            returnFlightTime4 = ObjectFactory().hiveBox.getReturnFlightTime4();
          })
        : setState(() {
            returnFlightTime4 = false;
          });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 135.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 1.2),
                child: Text(
                  'Departure flight from ' + widget.destinationFromName,
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context).hoverColor),
                ),
              )
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ResultPageBottomSheetFlightTimesTimeBoxWidget(
                time: '00:00 - 10:00',
                onClickStatus: departureFlightTime1,
                onTap: () {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    departureFlightTime1 == false
                        ? widget.departureFlightTime1(true)
                        : widget.departureFlightTime1(false);
                  });
                  setState(() {
                    departureFlightTime1 == false
                        ? departureFlightTime1 = true
                        : departureFlightTime1 = false;
                  });
                },
              ),
              ResultPageBottomSheetFlightTimesTimeBoxWidget(
                time: '10:00 - 16:00',
                onClickStatus: departureFlightTime2,
                onTap: () {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    departureFlightTime2 == false
                        ? widget.departureFlightTime2(true)
                        : widget.departureFlightTime2(false);
                  });
                  setState(() {
                    departureFlightTime2 == false
                        ? departureFlightTime2 = true
                        : departureFlightTime2 = false;
                  });
                },
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ResultPageBottomSheetFlightTimesTimeBoxWidget(
                time: '16:00 - 20:00',
                onClickStatus: departureFlightTime3,
                onTap: () {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    departureFlightTime3 == false
                        ? widget.departureFlightTime3(true)
                        : widget.departureFlightTime3(false);
                  });
                  setState(() {
                    departureFlightTime3 == false
                        ? departureFlightTime3 = true
                        : departureFlightTime3 = false;
                  });
                },
              ),
              ResultPageBottomSheetFlightTimesTimeBoxWidget(
                time: '20:00 - 00:00',
                onClickStatus: departureFlightTime4,
                onTap: () {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    departureFlightTime4 == false
                        ? widget.departureFlightTime4(true)
                        : widget.departureFlightTime4(false);
                  });
                  setState(() {
                    departureFlightTime4 == false
                        ? departureFlightTime4 = true
                        : departureFlightTime4 = false;
                  });
                },
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20.0),
          ),
          widget.roundTripStatus == true
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.2),
                          child: Text(
                            'Return flight from ' + widget.destinationToName,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                                fontSize: 14.0,
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w700,
                                color: Theme.of(context).hoverColor),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ResultPageBottomSheetFlightTimesTimeBoxWidget(
                          time: '00:00 - 10:00',
                          onClickStatus: returnFlightTime1,
                          onTap: () {
                            widget.showSaveButtonValueChanged(true);
                            setState(() {
                              returnFlightTime1 == false
                                  ? widget.returnFlightTime1(true)
                                  : widget.returnFlightTime1(false);
                            });
                            setState(() {
                              returnFlightTime1 == false
                                  ? returnFlightTime1 = true
                                  : returnFlightTime1 = false;
                            });
                          },
                        ),
                        ResultPageBottomSheetFlightTimesTimeBoxWidget(
                          time: '10:00 - 16:00',
                          onClickStatus: returnFlightTime2,
                          onTap: () {
                            widget.showSaveButtonValueChanged(true);
                            setState(() {
                              returnFlightTime2 == false
                                  ? widget.returnFlightTime2(true)
                                  : widget.returnFlightTime2(false);
                            });
                            setState(() {
                              returnFlightTime2 == false
                                  ? returnFlightTime2 = true
                                  : returnFlightTime2 = false;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ResultPageBottomSheetFlightTimesTimeBoxWidget(
                          time: '16:00 - 20:00',
                          onClickStatus: returnFlightTime3,
                          onTap: () {
                            widget.showSaveButtonValueChanged(true);
                            setState(() {
                              returnFlightTime3 == false
                                  ? widget.returnFlightTime3(true)
                                  : widget.returnFlightTime3(false);
                            });
                            setState(() {
                              returnFlightTime3 == false
                                  ? returnFlightTime3 = true
                                  : returnFlightTime3 = false;
                            });
                          },
                        ),
                        ResultPageBottomSheetFlightTimesTimeBoxWidget(
                          time: '20:00 - 00:00',
                          onClickStatus: returnFlightTime4,
                          onTap: () {
                            widget.showSaveButtonValueChanged(true);
                            setState(() {
                              returnFlightTime4 == false
                                  ? widget.returnFlightTime4(true)
                                  : widget.returnFlightTime4(false);
                            });
                            setState(() {
                              returnFlightTime4 == false
                                  ? returnFlightTime4 = true
                                  : returnFlightTime4 = false;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 15.0),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}
