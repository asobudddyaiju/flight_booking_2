import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetWidgetForStops extends StatefulWidget {
  ValueChanged showSaveButtonValueChanged;
  ValueChanged direct, tickOneStop, tickTwoPlusStops;
  ResultPageBottomSheetWidgetForStops(
      {this.showSaveButtonValueChanged,
      this.direct,
      this.tickOneStop,
      this.tickTwoPlusStops});
  @override
  _ResultPageBottomSheetWidgetForStopsState createState() =>
      _ResultPageBottomSheetWidgetForStopsState();
}

class _ResultPageBottomSheetWidgetForStopsState
    extends State<ResultPageBottomSheetWidgetForStops> {
  bool direct, tickOneStop, tickTwoPlusStops, showSaveButton;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            widget.showSaveButtonValueChanged(true);
            if (direct == false) {
              setState(() {
                widget.direct(true);
                direct = true;
              });
            } else if (((tickOneStop == true) || (tickTwoPlusStops == true)) &&
                (direct == true)) {
              setState(() {
                widget.direct(false);
                direct = false;
              });
            }
          },
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Direct',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).hoverColor),
                  ),
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                            color: Constants.kitGradients[0], width: 2.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: direct == true
                              ? Constants.kitGradients[0]
                              : Constants.kitGradients[11],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Divider(
          color: Constants.kitGradients[1],
        ),
        GestureDetector(
          onTap: () {
            widget.showSaveButtonValueChanged(true);
            if (tickOneStop == false) {
              setState(() {
                widget.tickOneStop(true);
                tickOneStop = true;
              });
            } else if (((direct == true) || (tickTwoPlusStops == true)) &&
                (tickOneStop == true)) {
              setState(() {
                widget.tickOneStop(false);
                tickOneStop = false;
              });
            }
          },
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    '1 Stop',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).hoverColor),
                  ),
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                            color: Constants.kitGradients[0], width: 2.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: tickOneStop == true
                              ? Constants.kitGradients[0]
                              : Constants.kitGradients[11],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Divider(
          color: Constants.kitGradients[1],
        ),
        GestureDetector(
          onTap: () {
            widget.showSaveButtonValueChanged(true);
            if (tickTwoPlusStops == false) {
              setState(() {
                widget.tickTwoPlusStops(true);
                tickTwoPlusStops = true;
              });
            } else if (((direct == true) || (tickOneStop == true)) &&
                (tickTwoPlusStops == true)) {
              setState(() {
                widget.tickTwoPlusStops(false);
                tickTwoPlusStops = false;
              });
            }
          },
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    '+2 Stops',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontWeight: FontWeight.w400,
                        color: Theme.of(context).hoverColor),
                  ),
                  Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                            color: Constants.kitGradients[0], width: 2.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: tickTwoPlusStops == true
                              ? Constants.kitGradients[0]
                              : Constants.kitGradients[11],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 20),
        )
      ],
    );
  }
}
