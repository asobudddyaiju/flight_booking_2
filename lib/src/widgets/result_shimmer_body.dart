import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ResultShimmerBody extends StatefulWidget {
  final   bool showPriceTapShimmer;
  ResultShimmerBody({this.showPriceTapShimmer});
  @override
  _ResultShimmerBodyState createState() => _ResultShimmerBodyState();
}

class _ResultShimmerBodyState extends State<ResultShimmerBody> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Shimmer.fromColors(
          baseColor: Theme.of(context).primaryColor,
          highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
          enabled: _enabled,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 35),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 85.0),
              ),
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 45),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),
            ],
          ),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 7),
        ),
        Shimmer.fromColors(
          baseColor: Theme.of(context).primaryColor,
          highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 65),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),
             Icon(Icons.arrow_forward,color: Constants.kitGradients[1],size: 20.0,),
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 65),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),
            ],
          ),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 7),
        ),
        Shimmer.fromColors(
          baseColor: Theme.of(context).primaryColor,
          highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 35),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 85.0),
              ),
              Container(
                width: screenWidth(context, dividedBy: 8),
                height: screenHeight(context, dividedBy: 45),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(6.0)),
              ),

            ],
          ),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 7),
        ),
        widget.showPriceTapShimmer == true?Shimmer.fromColors(
          baseColor: Theme.of(context).primaryColor,
          highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
          child: Container(
            width: screenWidth(context, dividedBy: 8),
            height: screenHeight(context, dividedBy: 20),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(6.0)),
          ),
        ):Container(),
      ],
    );
  }
}
