import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ResultShimmerBodyPart extends StatefulWidget {
  @override
  _ResultShimmerBodyPartState createState() => _ResultShimmerBodyPartState();
}

class _ResultShimmerBodyPartState extends State<ResultShimmerBodyPart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Spacer(
            flex: 1,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Shimmer.fromColors(
                baseColor: Theme.of(context).primaryColor,
                highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
                child: Container(
                  width: screenWidth(context, dividedBy: 8),
                  height: screenHeight(context, dividedBy: 35),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.greenAccent),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 250),
              ),
              Shimmer.fromColors(
                baseColor: Theme.of(context).primaryColor,
                highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
                child: Container(
                  width: screenWidth(context, dividedBy: 9),
                  height: screenHeight(context, dividedBy: 40),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.greenAccent),
                ),
              ),
            ],
          ),
          Spacer(
            flex: 2,
          ),
          Container(
            width: screenWidth(context, dividedBy: 2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Shimmer.fromColors(
                      baseColor: Theme.of(context).primaryColor,
                      highlightColor:
                          Theme.of(context).primaryColor.withOpacity(0.5),
                      child: Container(
                        width: screenWidth(context, dividedBy: 8),
                        height: screenHeight(context, dividedBy: 35),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: Colors.greenAccent),
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[24],
                          shape: BoxShape.circle),
                    ),
                    Container(
                        height: 1,
                        width: screenWidth(context, dividedBy: 5),
                        color: Constants.kitGradients[24]),
                    Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[24],
                          shape: BoxShape.circle),
                    ),
                    Container(
                        height: 1,
                        width: screenWidth(context, dividedBy: 5),
                        color: Constants.kitGradients[24]),
                    Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[24],
                          shape: BoxShape.circle),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Shimmer.fromColors(
                      baseColor: Theme.of(context).primaryColor,
                      highlightColor:
                          Theme.of(context).primaryColor.withOpacity(0.5),
                      child: Container(
                        width: screenWidth(context, dividedBy: 6),
                        height: screenHeight(context, dividedBy: 35),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: Colors.greenAccent),
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Spacer(
            flex: 2,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Shimmer.fromColors(
                baseColor: Theme.of(context).primaryColor,
                highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
                child: Container(
                  width: screenWidth(context, dividedBy: 8),
                  height: screenHeight(context, dividedBy: 35),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.greenAccent),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 250),
              ),
              Shimmer.fromColors(
                baseColor: Theme.of(context).primaryColor,
                highlightColor: Theme.of(context).primaryColor.withOpacity(0.5),
                child: Container(
                  width: screenWidth(context, dividedBy: 9),
                  height: screenHeight(context, dividedBy: 40),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.greenAccent),
                ),
              ),
            ],
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
