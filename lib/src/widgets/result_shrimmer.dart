import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_shimmer_body_part.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ResultShrimmer extends StatefulWidget {
  bool roundTripStatus;
  String route, dateTimePassengers;

  ResultShrimmer({this.roundTripStatus, this.dateTimePassengers, this.route});

  @override
  _ResultShrimmerState createState() => _ResultShrimmerState();
}

class _ResultShrimmerState extends State<ResultShrimmer> {
  ScrollController _controller = ScrollController();
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      // appBar: AppBar(
      //   backgroundColor: Constants.kitGradients[3],
      //   leading: Container(),
      //   toolbarHeight: screenHeight(context, dividedBy: 6.2),
      //   actions: [
      //     ResultAppBarWidget(
      //       routeFrom: widget.routeFrom,
      //       routeTo: widget.routeTo,
      //       dateTimePassengers: widget.roundTripStatus == true
      //           ? (widget.dateTimePassengersDayInAlphabet +
      //           " " +
      //           widget.dateTimePassengers +
      //           " - " +
      //           widget.dateTimePassengersDayReturnInAlphabet +
      //           " " +
      //           widget.dateTimeReturnPassengers +
      //           ", " +
      //           widget.passengerCount +
      //           " " +
      //           getTranslated(context, "passengers"))
      //           : (widget.dateTimePassengersDayInAlphabet +
      //           " " +
      //           widget.dateTimePassengers +
      //           ", " +
      //           widget.passengerCount +
      //           " " +
      //           getTranslated(context, "passengers")),
      //     ),
      //   ],
      // ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getTranslated(context, "Searching"),
                  style: TextStyle(
                    fontFamily: 'AirbnbCerealAppRegular',
                    color: Constants.kitGradients[1],
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                controller: _controller,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: 10,
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                          color: Theme.of(context).cardColor,
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Shimmer.fromColors(
                                  baseColor: Theme.of(context).primaryColor,
                                  highlightColor: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.5),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 15),
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        color: Colors.greenAccent),
                                  ),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Shimmer.fromColors(
                                  baseColor: Theme.of(context).primaryColor,
                                  highlightColor: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.5),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 15),
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        color: Colors.greenAccent),
                                  ),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Shimmer.fromColors(
                                  baseColor: Theme.of(context).primaryColor,
                                  highlightColor: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.5),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 8),
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        color: Colors.greenAccent),
                                  ),
                                ),
                              ],
                            ),
                            Divider(
                              color: Constants.kitGradients[24],
                            ),
                            // ResultShimmerBody(
                            //   showPriceTapShimmer:
                            //       widget.roundTripStatus == true ? false : true,
                            // ),
                            ResultShimmerBodyPart(),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            widget.roundTripStatus == true
                                ? ResultShimmerBodyPart()
                                : Container(),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Row(
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 20),
                                  height: screenWidth(context, dividedBy: 20),
                                  child: Image.asset(
                                    "assets/icons/price_tag.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Shimmer.fromColors(
                                  baseColor: Theme.of(context).primaryColor,
                                  highlightColor: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.5),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 2.8),
                                    height: screenWidth(context, dividedBy: 20),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).primaryColor,
                                        borderRadius:
                                            BorderRadius.circular(6.0)),
                                  ),
                                ),
                                Spacer(
                                  flex: 30,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
