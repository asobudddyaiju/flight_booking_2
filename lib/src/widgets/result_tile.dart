import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/currency_symbol.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ResultsPageTile extends StatefulWidget {
  final String oneWayDepartureTime;
  final String oneWayArrivalTime;
  final String oneWayDepartureAirportName;
  final String oneWayArrivalAirportName;
  final String oneWayTotalTime;
  final String oneWayStopsNumber;
  final String roundTripDepartureTime;
  final String roundTripArrivalTime;
  final String roundTripDepartureAirportName;
  final String roundTripAirportName;
  final String roundTripTotalTime;
  final String roundTripStopsNumber;
  final String price;
  final bool roundTripStatus;
  final String offersFound;
  final Function onTap;
  final List<dynamic> resultPageAirlineArrayByIndex;

  ResultsPageTile(
      {this.oneWayDepartureTime,
      this.onTap,
      this.oneWayDepartureAirportName,
      this.oneWayArrivalAirportName,
      this.oneWayArrivalTime,
      this.oneWayStopsNumber,
      this.oneWayTotalTime,
      this.roundTripAirportName,
      this.roundTripArrivalTime,
      this.roundTripDepartureAirportName,
      this.roundTripDepartureTime,
      this.roundTripStopsNumber,
      this.roundTripTotalTime,
      this.price,
      this.roundTripStatus,
      this.offersFound,
      this.resultPageAirlineArrayByIndex});

  @override
  _ResultsPageTileState createState() => _ResultsPageTileState();
}

class _ResultsPageTileState extends State<ResultsPageTile> {
  int resultPageAirlineArrayByIndexLength;
  @override
  void initState() {
    setState(() {
      resultPageAirlineArrayByIndexLength =
          widget.resultPageAirlineArrayByIndex.length;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: GestureDetector(
        onTap: () {
          widget.onTap();
        },
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: Theme.of(context).cardColor,
          child: Container(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  vertical: screenHeight(context, dividedBy: 80)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          height: screenHeight(context, dividedBy: 30),
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: resultPageAirlineArrayByIndexLength,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: EdgeInsets.only(right: 5.0),
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 16),
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(2.5),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                "https://images.kiwi.com/airlines/64/${widget.resultPageAirlineArrayByIndex[index]}.png"),
                                            fit: BoxFit.fill)),
                                  ),
                                );
                              })),
                      Container(
                        width: screenWidth(context, dividedBy: 3.5),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                (ObjectFactory().hiveBox.hiveGet(
                                                key: "currency_name") !=
                                            null
                                        ? (getCurrencySymbol(
                                            code: ObjectFactory()
                                                .hiveBox
                                                .hiveGet(key: "currency_name")
                                                .split(" ")
                                                .first))
                                        : ("\$")) +
                                    widget.price,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Constants.kitGradients[4],
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1,
                    color: Constants.kitGradients[2],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Spacer(
                          flex: 1,
                        ),
                        Container(
                          // width: screenWidth(context, dividedBy: 7),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.oneWayDepartureTime,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text(
                                widget.oneWayDepartureAirportName,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Theme.of(context).primaryColorLight,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        Spacer(
                          flex: 2,
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1.5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                widget.oneWayStopsNumber == "0"
                                    ? "Direct"
                                    : widget.oneWayStopsNumber + " Stops",
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical:
                                        screenHeight(context, dividedBy: 320)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Spacer(
                                      flex: 1,
                                    ),
                                    Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                          color: Constants.kitGradients[24],
                                          shape: BoxShape.circle),
                                    ),
                                    Container(
                                        height: 1,
                                        width:
                                            screenWidth(context, dividedBy: 5),
                                        color: Constants.kitGradients[24]),
                                    Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                          color: Constants.kitGradients[24],
                                          shape: BoxShape.circle),
                                    ),
                                    Container(
                                        height: 1,
                                        width:
                                            screenWidth(context, dividedBy: 5),
                                        color: Constants.kitGradients[24]),
                                    Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                          color: Constants.kitGradients[24],
                                          shape: BoxShape.circle),
                                    ),
                                    Spacer(
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                widget.oneWayTotalTime,
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Theme.of(context).primaryColorLight,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        Spacer(
                          flex: 2,
                        ),
                        Container(
                          // width: screenWidth(context, dividedBy: 7),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.oneWayArrivalTime,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text(
                                widget.oneWayArrivalAirportName,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Theme.of(context).primaryColorLight,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                  widget.roundTripStatus == true
                      ? Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 40),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Spacer(
                                    flex: 1,
                                  ),
                                  Container(
                                    // width: screenWidth(context, dividedBy: 7),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          widget.roundTripDepartureTime,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w700),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 100),
                                        ),
                                        Text(
                                          widget.roundTripDepartureAirportName,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Theme.of(context)
                                                  .primaryColorLight,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(
                                    flex: 2,
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 1.5),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          widget.roundTripStopsNumber == "0"
                                              ? "Direct"
                                              : widget.roundTripStopsNumber +
                                                  " Stops",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Colors.black,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: screenHeight(context,
                                                  dividedBy: 320)),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Spacer(
                                                flex: 1,
                                              ),
                                              Container(
                                                width: 10,
                                                height: 10,
                                                decoration: BoxDecoration(
                                                    color: Constants
                                                        .kitGradients[24],
                                                    shape: BoxShape.circle),
                                              ),
                                              Container(
                                                  height: 1,
                                                  width: screenWidth(context,
                                                      dividedBy: 5),
                                                  color: Constants
                                                      .kitGradients[24]),
                                              Container(
                                                width: 10,
                                                height: 10,
                                                decoration: BoxDecoration(
                                                    color: Constants
                                                        .kitGradients[24],
                                                    shape: BoxShape.circle),
                                              ),
                                              Container(
                                                  height: 1,
                                                  width: screenWidth(context,
                                                      dividedBy: 5),
                                                  color: Constants
                                                      .kitGradients[24]),
                                              Container(
                                                width: 10,
                                                height: 10,
                                                decoration: BoxDecoration(
                                                    color: Constants
                                                        .kitGradients[24],
                                                    shape: BoxShape.circle),
                                              ),
                                              Spacer(
                                                flex: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Text(
                                          widget.roundTripTotalTime,
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Theme.of(context)
                                                  .primaryColorLight,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(
                                    flex: 2,
                                  ),
                                  Container(
                                    // width: screenWidth(context, dividedBy: 7),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          widget.roundTripArrivalTime,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w700),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 100),
                                        ),
                                        Text(
                                          widget.roundTripAirportName,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Theme.of(context)
                                                  .primaryColorLight,
                                              fontFamily:
                                                  'AirbnbCerealAppRegular',
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  Row(
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 20),
                        height: screenWidth(context, dividedBy: 20),
                        child: SvgPicture.asset(
                          "assets/icons/offers.svg",
                          fit: BoxFit.fill,
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Container(
                        // width: screenWidth(context, dividedBy: 2.8),
                        height: screenWidth(context, dividedBy: 20),
                        child: Text(
                          widget.offersFound + " offer from kiwi.com",
                          style: TextStyle(
                              color: Theme.of(context).primaryColorLight,
                              fontFamily: 'AirbnbCerealAppRegular',
                              fontSize: screenWidth(context, dividedBy: 40),
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      Spacer(
                        flex: 30,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
